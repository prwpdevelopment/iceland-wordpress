<?php

/**
 * Copyright (c) 29/10/2016.
 * Theme Name: iceland
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */
class Wpbucket_vc_mapping
{
    static function wpbucket_vc_map()
    {
        /*
         * VC MAP FOR HEADING
         * */
        vc_map(array(
            "name" => __("Heading", "wpbucket"),
            "base" => "wpbucket_heading",
            "class" => "",
            "category" => __("Iceland", "wpbucket"),
            /* 'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
             'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),*/
            "params" => array(
                array(
                    "type" => "textfield",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Primary Title", "wpbucket"),
                    "param_name" => "primary_title",
                    "value" => __("Alerts & Buttons", "wpbucket"),
                    'save_always' => true,
                    "description" => __("Primary heading shows before secondary heading, leave empty to eleminate", "wpbucket")
                ),
                array(
                    "type" => "textfield",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Secondary Title", "wpbucket"),
                    "param_name" => "secondary_title",
                    "value" => __("Island is an outstanding company who need gardening!", "wpbucket"),
                    'save_always' => true,
                    "description" => __("Secondary heading shows before primary heading, leave empty to eliminate", "wpbucket")
                )
            )
        ));

        /*
         * VC MAP FOR HEADING WITH ICON
         * */
        vc_map(array(
            "name" => __("Heading With Icon", "wpbucket"),
            "base" => "wpbucket_heading_icon",
            "class" => "",
            "category" => __("Iceland", "wpbucket"),
            /* 'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
             'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),*/
            "params" => array(
                array(
                    "type" => "textfield",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Primary Title", "wpbucket"),
                    "param_name" => "primary_title",
                    "value" => __("Alerts & Buttons", "wpbucket"),
                    'save_always' => true,
                    "description" => __("Primary heading shows before secondary heading, leave empty to eleminate", "wpbucket")
                ),
                array(
                    "type" => "textfield",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Secondary Title", "wpbucket"),
                    "param_name" => "secondary_title",
                    "value" => __("Island is an outstanding company who need gardening!", "wpbucket"),
                    'save_always' => true,
                    "description" => __("Secondary heading shows before primary heading, leave empty to eliminate", "wpbucket")
                ),
                array(
                    "type" => "wpbucket_flat_icon",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Choose Icon", "wpbucket"),
                    "param_name" => "heading_icon",
                    "value" => '',
                    'save_always' => true,
                    "description" => __("Icon shows left side of main title.", "wpbucket")
                ),
            )
        ));

        /*
         * VC MAP FOR THEME BUTTONS
         *
         * */

        vc_map(array(
            "name" => __("Iceland Buttons", "wpbucket"),
            "base" => "wpbucket_buttons",
            "class" => "",
            "category" => __("Iceland", "wpbucket"),
            /* 'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
             'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),*/
            "params" => array(
                array(
                    "type" => "textfield",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Button Title", "wpbucket"),
                    "param_name" => "button_title",
                    "value" => __("Button", "wpbucket"),
                    'save_always' => true,
                    "description" => __("This title is used for button text", "wpbucket")
                ),
                array(
                    "type" => "vc_link",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Button Link", "wpbucket"),
                    "param_name" => "button_link",
                    "value" => __("#", "wpbucket"),
                    'save_always' => true,
                    "description" => __("Use button link for a tag href attributes", "wpbucket")
                ),
                array(
                    "type" => "dropdown",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Button Type", "wpbucket"),
                    "param_name" => "button_type",
                    "value" => array(
                        'Primary Type' => 'btn-primary',
                        'Default Type' => 'btn-default'
                    ),
                    "std" => 'btn-default',
                    'save_always' => true,
                    "description" => __("Select your button types.", "wpbucket")
                ),
                array(
                    "type" => "dropdown",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Button Size", "wpbucket"),
                    "param_name" => "button_size",
                    "value" => array(
                        'Full Width' => 'btn-block',
                        'Large' => 'btn-lg',
                        'Small' => 'btn-sm',
                        'Extra Small' => 'btn-xs',
                        'Default' => 'no-value'
                    ),
                    "std" => 'btn-sm',
                    'save_always' => true,
                    "description" => __("Select your button sizes.", "wpbucket")
                ),
                array(
                    "type" => "wpbucket_icon",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Button Icon", "wpbucket"),
                    "param_name" => "button_icon",
                    'save_always' => true,
                    "description" => __("This icon is used for button. Leave empty if you do not need.", "wpbucket")
                ),
            )
        ));

        /*
         * VC MAPPING FOR ALERTS
         *
         * */
        vc_map(array(
            "name" => __("Iceland Alert Notice", "wpbucket"),
            "base" => "wpbucket_alert",
            "class" => "",
            "category" => __("Iceland", "wpbucket"),
            /* 'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
             'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),*/
            "params" => array(
                array(
                    "type" => "textfield",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Strong Title", "wpbucket"),
                    "param_name" => "alert_title",
                    "value" => __("Well done!", "wpbucket"),
                    'save_always' => true,
                    "description" => __("This title display as Bold in a sentence", "wpbucket")
                ),
                array(
                    "type" => "textfield",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Normal Title", "wpbucket"),
                    "param_name" => "alert_sub_title",
                    "value" => __("You successfully read this important alert message.", "wpbucket"),
                    'save_always' => true,
                    "description" => __("This title display as normal font in a sentence after bold sentence.", "wpbucket")
                ),
                array(
                    "type" => "dropdown",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Alert Type", "wpbucket"),
                    "param_name" => "alert_type",
                    "value" => array(
                        'Success Type' => 'alert-success',
                        'Info Type' => 'alert-info',
                        'Warning Type' => 'alert-warning',
                        'Danger Type' => 'alert-danger'
                    ),
                    "std" => 'alert-success',
                    'save_always' => true,
                    "description" => __("Select your alert types.", "wpbucket")
                )
            )
        ));
        /*
         * VC MAPING FOR TESTIMONIAL
         * */
        vc_map(array(
            "name" => __("Iceland Testimonial", "wpbucket"),
            "base" => "wpbucket_testimonial",
            "class" => "",
            "category" => __("Iceland", "wpbucket"),
            /* 'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
             'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),*/
            "params" => array(
                array(
                    "type" => "textfield",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Title", "wpbucket"),
                    "param_name" => "testimonial_title",
                    "value" => __("Thanks for support us!", "wpbucket"),
                    'save_always' => true,
                    "description" => __("This title display before auhtor name.", "wpbucket")
                ),
                array(
                    "type" => "textfield",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Name", "wpbucket"),
                    "param_name" => "testimonial_name",
                    "value" => __("Jackie Anderson", "wpbucket"),
                    'save_always' => true,
                    "description" => __("Name of the author who gave this testimonial.", "wpbucket")
                ),
                array(
                    "type" => "textarea",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Testimonial Paragraph", "wpbucket"),
                    "param_name" => "testimonial_para",
                    "value" => __("When you need a gardener we are here for help with professional gardeners team. For more information and about prices, please visit our services page.", "wpbucket"),
                    'save_always' => true,
                    "description" => __("Description about testimonial.", "wpbucket")
                ),
                array(
                    "type" => "attach_image",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Picture", "wpbucket"),
                    "param_name" => "testimonial_picture",
                    'save_always' => true,
                    "description" => __("Author Picture, leave empty if author has no picture.", "wpbucket")
                ),
                array(
                    "type" => "colorpicker",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Background Color", "wpbucket"),
                    "param_name" => "testimonial_bg",
                    "value" => '#ffffff',
                    'save_always' => true,
                    "description" => __("Choose color that is suitable for testimonial background color.", "wpbucket")
                )
            )
        ));

        /*
         * VC MAPING FOR CONTACT INFO DETAILS
         * */
        vc_map(array(
                "name" => __("Contact Details", "wpbucket"),
                "base" => "contact_base_details",
                "category" => __("Iceland", "wpbucket"),
                "as_parent" => array('only' => 'wpbucket_contact_details'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
                "content_element" => true,
                "show_settings_on_create" => false,
                "is_container" => false,
                "js_view" => 'VcColumnView'
            )
        );
        vc_map(array(
            "name" => __("Contact Details", "wpbucket"),
            "base" => "wpbucket_contact_details",
            "class" => "",
            "content_element" => true,
            "category" => __("Iceland", "wpbucket"),
            "as_child" => array('only' => 'contact_base_details'),
            "params" => array(
                array(
                    "type" => "textfield",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Question", "wpbucket"),
                    "param_name" => "contact_question",
                    "value" => __("Phone", "wpbucket"),
                    'save_always' => true,
                    "description" => __("This question is that, client asked for, like what is your Phone number?.", "wpbucket")
                ),
                array(
                    "type" => "textfield",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Answer", "wpbucket"),
                    "param_name" => "contact_answer",
                    "value" => __("+90 534 970 00 00", "wpbucket"),
                    'save_always' => true,
                    "description" => __("Answer of the asked question, like phone number is +90 534 970 00 00.", "wpbucket")
                ),
                array(
                    "type" => "wpbucket_icon",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Choose Icon", "wpbucket"),
                    "param_name" => "contact_icon",
                    "value" => '',
                    'save_always' => true,
                    "description" => __("Choose icon that is suitable for asked question.", "wpbucket")
                )
            )
        ));

        /*
         * VC MAPPING FOR FEATURE BOXES
         * */
        vc_map(array(
            "name" => __("Feature Box", "wpbucket"),
            "base" => "wpbucket_feature_box",
            "class" => "",
            "category" => __("Iceland", "wpbucket"),
            /* 'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
             'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),*/
            "params" => array(
                array(
                    "type" => "textfield",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Title", "wpbucket"),
                    "param_name" => "feature_box_title",
                    "value" => __("Afforestation", "wpbucket"),
                    'save_always' => true,
                    "description" => __("This title display after icon.", "wpbucket")
                ),
                array(
                    "type" => "textfield",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Description", "wpbucket"),
                    "param_name" => "feature_box_para",
                    "value" => __("Morbi gravida nisl ac purus vulputate, ac convallis tellus dapibus.", "wpbucket"),
                    'save_always' => true,
                    "description" => __("Give a description, it will show after title.", "wpbucket")
                ),
                array(
                    "type" => "wpbucket_flat_icon",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Choose Icon", "wpbucket"),
                    "param_name" => "feature_box_icon",
                    "value" => '',
                    'save_always' => true,
                    "description" => __("Icon show at top, before title and description..", "wpbucket")
                ),
                array(
                    "type" => "attach_image",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Image", "wpbucket"),
                    "param_name" => "feature_box_image",
                    'save_always' => true,
                    "description" => __("Set image for feature box.", "wpbucket")
                )
            )
        ));
        /*
         * VC MAPPING FOR SERVICE BOXES
         *
         * */
        vc_map(array(
            "name" => __("Service Box", "wpbucket"),
            "base" => "wpbucket_service_box",
            "class" => "",
            "category" => __("Iceland", "wpbucket"),
            /* 'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
             'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),*/
            "params" => array(
                array(
                    "type" => "textfield",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Title", "wpbucket"),
                    "param_name" => "service_box_title",
                    "value" => __("Experienced Staff", "wpbucket"),
                    'save_always' => true,
                    "description" => __("This title display after Image.", "wpbucket")
                ),
                array(
                    "type" => "textfield",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Description", "wpbucket"),
                    "param_name" => "service_box_para",
                    "value" => __("With 20 years of experience of our gardeners are now more authentic landscaping design.", "wpbucket"),
                    'save_always' => true,
                    "description" => __("Give a description, it will show after title.", "wpbucket")
                ),
                array(
                    "type" => "wpbucket_flat_icon",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Choose Icon", "wpbucket"),
                    "param_name" => "service_box_icon",
                    "value" => '',
                    'save_always' => true,
                    "description" => __("Icon show left side of title.", "wpbucket")
                ),
                array(
                    "type" => "attach_image",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Image", "wpbucket"),
                    "param_name" => "service_box_image",
                    'save_always' => true,
                    "description" => __("Set image for service box.", "wpbucket")
                )
            )
        ));

        /*
         * VC MAPPING FOR PRICE TABLE
         * */
        vc_map(array(
            "name" => __("Price Table", "wpbucket"),
            "base" => "wpbucket_price_table",
            "class" => "",
            "category" => __("Iceland", "wpbucket"),
            /* 'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
             'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),*/
            "params" => array(
                array(
                    "type" => "textfield",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Title", "wpbucket"),
                    "param_name" => "pricetable_title",
                    "value" => __("Beginner Package", "wpbucket"),
                    'save_always' => true,
                    "description" => __("This title display after currency.", "wpbucket")
                ),
                array(
                    "type" => "textfield",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Currency Symbol", "wpbucket"),
                    "param_name" => "currency_symbol",
                    "value" => __("$", "wpbucket"),
                    'save_always' => true,
                    "description" => __("Currency symbol like '$' doller.", "wpbucket")
                ),
                array(
                    "type" => "textfield",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Amount", "wpbucket"),
                    "param_name" => "amount_price_table",
                    "value" => '100',
                    'save_always' => true,
                    "description" => __("Set amount fot this price table.", "wpbucket")
                ),
                array(
                    "type" => "textarea_html",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Lists", "wpbucket"),
                    "param_name" => "content",
                    'save_always' => true,
                    "description" => __("Make a list for price table", "wpbucket")
                ),
                 array(
                     "type" => "textfield",
                     "holder" => "div",
                     "class" => "",
                     "heading" => __("Button Text", "wpbucket"),
                     "param_name" => "button_text_price_table",
                     "value" => __("Order Now", "wpbucket"),
                     'save_always' => true,
                     "description" => __("", "wpbucket")
                 ),
                array(
                    "type" => "vc_link",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Button URL", "wpbucket"),
                    "param_name" => "button_url_price_table",
                    "value" => __("#", "wpbucket"),
                    'save_always' => true,
                    "description" => __("Give your button url, like paypal url etc", "wpbucket")
                ),
                array(
                    "type" => "dropdown",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Highlight", "wpbucket"),
                    "param_name" => "higilight_price_table",
                    "value" => array(
                        'None' => 'none',
                        'Left-Top-Corner' => 'rightbg',
                        'Right-Bottom-Corner' => 'leftbg',
                    ),
                    "std" => 'none',
                    'save_always' => true,
                    "description" => __("Select if price table highlight or not.", "wpbucket")
                ),
                array(
                    "type" => "dropdown",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Content Alignment", "wpbucket"),
                    "param_name" => "content_alignment",
                    "value" => array(
                        'Left' => 'text-left',
                        'Right' => 'text-right',
                        'Center' => 'text-center',
                    ),
                    "std" => 'text-center',
                    'save_always' => true,
                    "description" => __("Select alignment to display price table content n right way.", "wpbucket")
                )
            )
        ));
        /*
         * VC MAPPING FOR BLOG
         * */
        vc_map(array(
            "name" => __("Blog", "wpbucket"),
            "base" => "wpbucket_blog",
            "class" => "",
            "category" => __("Iceland", "wpbucket"),
            /* 'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
             'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),*/
            "params" => array(
                array(
                    "type" => "wpbucket_get_posts_field",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Choose Post", "wpbucket"),
                    "param_name" => "wpbucket_choose_post",
                    'save_always' => true,
                    "description" => __("Select a post to display into selected column.", "wpbucket")
                )
            )
        ));
        /*
         * VC MAPPING FOR BANNER
         * */
        vc_map(array(
            "name" => __("Banner", "wpbucket"),
            "base" => "wpbucket_banner",
            "class" => "",
            "category" => __("Iceland", "wpbucket"),
            /* 'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
             'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),*/
            "params" => array(
                array(
                    "type" => "textarea_html",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Text for banner", "wpbucket"),
                    "param_name" => "content",
                    'save_always' => true,
                    "description" => __("Write a sentence for banner, if you would like to highlight some of words then wrap with 'mark' tag. ", "wpbucket")
                )
            )
        ));
        /*
         * VC MAPPING FOR OUR GARDENERS
         * */
        vc_map(array(
            "name" => __("Gardeners", "wpbucket"),
            "base" => "wpbucket_gardeners",
            "class" => "",
            "category" => __("Iceland", "wpbucket"),
            /* 'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
             'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),*/
            "params" => array(
                array(
                    "type" => "textfield",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Designation", "wpbucket"),
                    "param_name" => "gardener_designation",
                    "value" => __("CEO/Founder", "wpbucket"),
                    'save_always' => true,
                    "description" => __("Write down designation of your team member", "wpbucket")
                ),
                array(
                    "type" => "textfield",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Name", "wpbucket"),
                    "param_name" => "gardener_name",
                    "value" => __("Martin Martines", "wpbucket"),
                    'save_always' => true,
                    "description" => __("Write down name of your team member", "wpbucket")
                ),
                array(
                    "type" => "textarea",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Description", "wpbucket"),
                    "param_name" => "gardener_para",
                    "value" => __("Howdy, I am Martin from England, I am founder, CEO and working at Iceland company.", "wpbucket"),
                    'save_always' => true,
                    "description" => __("Write down short description of your team member", "wpbucket")
                ),
                array(
                    "type" => "attach_image",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Picture", "wpbucket"),
                    "param_name" => "gardener_picture",
                    'save_always' => true,
                    "description" => __("Upload your team member's photo", "wpbucket")
                ),
                array(
                    "type" => "exploded_textarea",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Social Icon", "wpbucket"),
                    "param_name" => "gardener_social_icon",
                    'save_always' => true,
                    "description" => __("Type fontawesome social font class name. Multiple classes enter in new line. Please follow the documentation.", "wpbucket").'<a href="http://fontawesome.io/icons/" target="_blank">Fontawesome</a>'
                ),
                array(
                    "type" => "exploded_textarea",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Social URL", "wpbucket"),
                    "param_name" => "gardener_social_url",
                    'save_always' => true,
                    "description" => __("Type social url. Multiple url enter in new line. Sequence must be same as Social Icon. If facebook icon is first position at Social Icon field then facebook url must be the first entry into this field. Both social icon and social url has same number of inputs. Please follow the documentation.", "wpbucket")
                )
            )
        ));
        /*
         * VC MAPPING FOR TABS SECTIONS
         * */
        vc_map(array(
                "name" => __("Tab Section", "wpbucket"),
                "base" => "wpbucket_tab_section_master",
                "category" => __("Iceland", "wpbucket"),
                "as_parent" => array('only' => 'wpbucket_tab_section_child'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
                "content_element" => true,
                "show_settings_on_create" => false,
                "is_container" => false,
                "js_view" => 'VcColumnView'
            )
        );
        vc_map(array(
            "name" => __("Tab Section", "wpbucket"),
            "base" => "wpbucket_tab_section_child",
            "class" => "",
            "content_element" => true,
            "category" => __("Iceland", "wpbucket"),
            "as_child" => array('only' => 'wpbucket_tab_section_master'),
            "params" => array(
                array(
                    "type" => "textfield",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Section Name", "wpbucket"),
                    "param_name" => "section_name",
                    "value" => __("Section 1", "wpbucket"),
                    'save_always' => true,
                    "description" => __("Give tab button name.", "wpbucket")
                ),
                array(
                    "type" => "textfield",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Section ID", "wpbucket"),
                    "param_name" => "section_id",
                    "value" => __("section1", "wpbucket"),
                    'save_always' => true,
                    "description" => __("Becarefull about section id's uniqueness. This id is needed when section content created.", "wpbucket")
                ),
                array(
                    "type" => "dropdown",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Section Active", "wpbucket"),
                    "param_name" => "section_active_inactive",
                    "value" => array(
                        'Inactive' => '',
                        'Active' => 'active',
                    ),
                    "std" => '',
                    'save_always' => true,
                    "description" => __("Active or in active tab section button. Mind it which section is active, it will need when corresponding section content created..", "wpbucket")
                )
            )
        ));
        /*
         * VC MAPPING FOR TABS SECTIONS CONTENT
         * */
        vc_map(array(
                "name" => __("Tab Content", "wpbucket"),
                "base" => "wpbucket_tab_content_master",
                "category" => __("Iceland", "wpbucket"),
                "as_parent" => array('only' => 'wpbucket_tab_content_child'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
                "content_element" => true,
                "show_settings_on_create" => false,
                "is_container" => false,
                "js_view" => 'VcColumnView'
            )
        );
        vc_map(array(
            "name" => __("Tab Content", "wpbucket"),
            "base" => "wpbucket_tab_content_child",
            "class" => "",
            "content_element" => true,
            "category" => __("Iceland", "wpbucket"),
            "as_child" => array('only' => 'wpbucket_tab_content_master'),
            "params" => array(
                array(
                    "type" => "attach_images",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Content Images", "wpbucket"),
                    "param_name" => "section_images",
                    'save_always' => true,
                    "description" => __("Upload tab content images, leave empty if no image need.", "wpbucket")
                ),
                array(
                    "type" => "textarea_html",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Content", "wpbucket"),
                    "param_name" => "content",
                    'save_always' => true,
                    "description" => __("Give the content.", "wpbucket")
                ),
                array(
                    "type" => "textfield",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Content ID", "wpbucket"),
                    "param_name" => "content_id",
                    "value" => __("section1", "wpbucket"),
                    'save_always' => true,
                    "description" => __("Becarefull about content ID's uniqueness. This ID is same as section ID.", "wpbucket")
                ),
                array(
                    "type" => "dropdown",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Section Active", "wpbucket"),
                    "param_name" => "content_active_inactive",
                    "value" => array(
                        'Inactive' => '',
                        'Active' => 'active in',
                    ),
                    "std" => '',
                    'save_always' => true,
                    "description" => __("Make it active if it's corresponding tab section, which ID are same, is active.", "wpbucket")
                )
            )
        ));

        /*
         * VC MAPPING FOR ICELAND CONTENT.
         * */

        vc_map(array(
            "name" => __("Icelan content", "wpbucket"),
            "base" => "wpbucket_iceland_content",
            "class" => "",
            "category" => __("Iceland", "wpbucket"),
            "params" => array(
                array(
                    "type" => "textfield",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Heading", "wpbucket"),
                    "param_name" => "iceland_heading",
                    "value" => __("We are Iceland Group", "wpbucket"),
                    'save_always' => true,
                    "description" => __("This field is responsible to display main heading for the content. Leave empty if you do no need.", "wpbucket")
                ),
                array(
                    "type" => "textfield",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Sub Heading", "wpbucket"),
                    "param_name" => "iceland_sub_heading",
                    "value" => __("A Professional Gardening Service for Ever!", "wpbucket"),
                    'save_always' => true,
                    "description" => __("This field is responsible to display, after the main heading, as a second heading for the content. Leave empty if you do not need", "wpbucket")
                ),
                array(
                    "type" => "textarea_html",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Content", "wpbucket"),
                    "param_name" => "content",
                    'save_always' => true,
                    "description" => __("Give the content.", "wpbucket")
                ),
                array(
                    "type" => "textfield",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Button Text", "wpbucket"),
                    "param_name" => "iceland_button_text",
                    "value" => __("Read More", "wpbucket"),
                    'save_always' => true,
                    "description" => __("This field is responsible to display the text of the button. Leave empty if you do not need button.", "wpbucket")
                ),
                array(
                    "type" => "vc_link",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Button URL", "wpbucket"),
                    "param_name" => "iceland_button_url",
                    "value" => __("#", "wpbucket"),
                    'save_always' => true,
                    "description" => __("This field is responsible for button url.", "wpbucket")
                )
            )
        ));

        /*
         * VC MAPPING FOR PRODUCT PAGE
         *
         * */
        vc_map(array(
            "name" => __("Product", "wpbucket"),
            "base" => "wpbucket_product",
            "class" => "",
            "category" => __("Iceland", "wpbucket"),
            /* 'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
             'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),*/
            "params" => array(
                array(
                    "type" => "wpbucket_get_products_field",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Choose Product", "wpbucket"),
                    "param_name" => "wpbucket_choose_product",
                    'save_always' => true,
                    "description" => __("Select a product to display into selected column.", "wpbucket")
                )
            )
        ));
    }
}