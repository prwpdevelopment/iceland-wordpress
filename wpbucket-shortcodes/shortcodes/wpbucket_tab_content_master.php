<?php
/**
 * Copyright (c) 02/11/2016.
 * Theme Name: wpbucket-shortcodes
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */
if (!function_exists('wpbucket_tab_content_master')) {
    function wpbucket_tab_content_master($atts, $content)
    {
        return '<div class="tab-content">'.do_shortcode($content).'</div>';
    }
}

if (!function_exists('wpbucket_tab_content_child')) {
    function wpbucket_tab_content_child($atts, $content)
    {
        extract(shortcode_atts(array(
            'section_images' => '',
            'content_id' => '',
            'content_active_inactive' => '',
        ), $atts));

        if ($atts['section_images']!=null) {
            $images = explode(',',$atts['section_images']);
            $number_of_image = count($images);
            $column = 12/$number_of_image;
            $images = explode(',',$atts['section_images']);
            $img_html = '';
            foreach ($images as $image){
                $img = wp_get_attachment_image_src($image, 'full');
                $img = '<img src="' . esc_url($img[0]) . '" alt="" class="img-responsive img-rounded">';
                $img_html .='<div class="col-md-'.esc_attr($column).'">
                                        <div class="about-widget">
                                            <div class="post-media entry">
                                                '.$img.'
                                                <div class="magnifier">
                                                </div><!-- end magnifier -->
                                            </div><!-- end media -->
                                        </div><!-- end about-widget -->
                                    </div>';
            }
        }else{
            $img_html = '';
        }

        return '<div class="tab-pane fade '.esc_attr($atts['content_active_inactive']).'" id="'.esc_attr($atts['content_id']).'">
                                <div class="row">'.$img_html.'</div><hr class="invis">'.apply_filters('the_content',$content).'</div>';

    }
}