<?php
/**
 * Copyright (c) 05/11/2016.
 * Theme Name: wpbucket-shortcodes
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */
if (!function_exists('wpbucket_product')) {
    function wpbucket_product($atts, $content = null)
    {
        extract(shortcode_atts(array(
            'wpbucket_choose_product' => ''
        ), $atts));
        if ($atts['wpbucket_choose_product'] != null) {
            $ID = $atts['wpbucket_choose_product'];
            if (has_post_thumbnail($ID)) {
                $feat_image_url = wp_get_attachment_url(get_post_thumbnail_id($ID));
                $feat_image_url = '<img src="' . $feat_image_url . '" alt="" class="img-responsive img-rounded">';
            } else {
                $feat_image_url = '';
            }
            $product = WC()->product_factory->get_product($ID);
            return ' <div class="shop-wrapper">
                            <div class="post-media entry">
                               ' . $feat_image_url . '
                                <div class="magnifier">
                                    <div class="buttons">
                                        ' . apply_filters('woocommerce_loop_add_to_cart_link',
                sprintf('<a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s st"><span class="flaticon-shop"></span></a>',
                    esc_url($product->add_to_cart_url()),
                    esc_attr(isset($quantity) ? $quantity : 1),
                    esc_attr($product->id),
                    esc_attr($product->get_sku()),
                    esc_attr(isset($class) ? $class : 'button'),
                    esc_html($product->add_to_cart_text())
                )) . '
                                    </div>
                                </div>
                            </div><!-- end media -->
                            
                            <div class="shop-meta">
                                <small><a href="'.get_the_permalink($ID).'">'.$product->get_price_html().'</a></small>
                                <h4><a href="'.get_the_permalink($ID).'">'.get_the_title($ID).'</a></h4>
                            </div><!-- end shop-meta -->
                        </div>';
        } else {
            return 'No Product Selected';
        }
    }
}