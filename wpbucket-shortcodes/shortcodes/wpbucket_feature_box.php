<?php
/**
 * Copyright (c) 31/10/2016.
 * Theme Name: wpbucket-shortcodes
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */
if (!function_exists('wpbucket_feature_box')) {
    function wpbucket_feature_box($atts, $content = null)
    {
        extract(shortcode_atts(array(
            'feature_box_title' => '',
            'feature_box_para' => '',
            'feature_box_icon' => '',
            'feature_box_image' => '',
        ), $atts));
        if ($atts['feature_box_image'] != null) {
            $img = wp_get_attachment_image_src($atts['feature_box_image'],'full');
            $img ='background-image: url(' . ($img[0]) . ')';
        } else
            $img = '';

        return '<div class="hoverbox parallax parallax-off" data-stellar-background-ratio="1" style="'.$img.';">
                                <a href="#" class="box">
                                    <div class="box-content">
                                        <i class="' . esc_attr($atts['feature_box_icon']) . '"></i>
                                        <h2><span>' . esc_html($atts['feature_box_title']) . '</span></h2>
                                    </div>
                                    <div class="box-back">
                                        <p>' . $atts['feature_box_para'] . '</p>
                                    </div>
                                </a>
                            </div>';
    }
}