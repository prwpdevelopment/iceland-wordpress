<?php
/**
 * Copyright (c) 29/10/2016.
 * Theme Name: wpbucket-shortcodes
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */

if (!function_exists('wpbucket_buttons')) {
    function wpbucket_buttons($atts, $content = null)
    {
        extract(shortcode_atts(array(
            'button_title' => '',
            'button_link' => '#',
            'button_type' => '',
            'button_size' => '',
            'button_icon' => ''
        ), $atts));
        $button_icon ='';
        if ($atts['button_icon']!=null) {
            $button_icon = '<i class="fa '.esc_attr($atts['button_icon']).'"></i>';
        }
        $hrf =  vc_build_link($atts['button_link']);
        return '<a href="' . esc_url( $hrf['url']) . '" class="btn ' . esc_attr($atts['button_type']) . ' ' . esc_attr($atts['button_size']) . '">' . esc_html($atts['button_title']) . ''.$button_icon.'</a>';
    }
}