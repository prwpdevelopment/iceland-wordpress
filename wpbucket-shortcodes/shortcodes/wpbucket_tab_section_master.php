<?php
/**
 * Copyright (c) 02/11/2016.
 * Theme Name: wpbucket-shortcodes
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */
if (!function_exists('wpbucket_tab_section_master')) {
    function wpbucket_tab_section_master($atts, $content = null)
    {
        return ' <div class="message-box">
                <ul class="nav nav-pills nav-stacked myTabs">' . do_shortcode($content) . '</ul></div>';
    }
}

if (!function_exists('wpbucket_tab_section_child')) {
    function wpbucket_tab_section_child($atts, $content){
        extract(shortcode_atts(array(
            'section_name' => '',
            'section_id' => '',
            'section_active_inactive' => '',
        ), $atts));

        return '<li class="'.esc_attr($atts['section_active_inactive']).'"><a href="#'.esc_attr($atts['section_id']).'" data-toggle="pill">'.esc_attr($atts['section_name']).'</a></li>';
    }

}