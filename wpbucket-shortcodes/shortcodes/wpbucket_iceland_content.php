<?php
/**
 * Copyright (c) 05/11/2016.
 * Theme Name: wpbucket-shortcodes
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */
if (!function_exists('wpbucket_iceland_content')) {
    function wpbucket_iceland_content($atts, $content = null)
    {
        extract(shortcode_atts(array(
            'iceland_heading' => '',
            'iceland_sub_heading' => '',
            'iceland_button_text' => '',
            'iceland_button_url' => ''
        ), $atts));
        $primary_heading = '';
        $secondary_heading = '';
        $button = '';
        if ($atts['iceland_heading'] != null) {
            $primary_heading .= '<h3>' . $atts['iceland_heading'] . '</h3>';
        }
        if ($atts['iceland_sub_heading'] != null) {
            $secondary_heading .= '<p class="lead">' . $atts['iceland_sub_heading'] . '</p>';
        }

        if ($atts['iceland_button_text'] != null) {
            $hrf =  vc_build_link($atts['iceland_button_url']);
            $button = ' <a href="' . esc_url($hrf['url']) . '" class="btn btn-primary">' . $atts['iceland_button_text'] . '</a>';
        }

        return '<div class="about-widget">
                            <div class="big-title">
                               ' . $primary_heading . '
                            </div><!-- end title -->
                            ' . $secondary_heading . '
                            ' . preg_replace('#^<\/p>|<p>$#', '', do_shortcode($content)) . '
                            ' . $button . '
                        </div>';
    }
}