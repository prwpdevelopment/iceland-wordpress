<?php
/**
 * Copyright (c) 30/10/2016.
 * Theme Name: wpbucket-shortcodes
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */
if (!function_exists('wpbucket_testimonial')) {
    function wpbucket_testimonial($atts, $content = null)
    {
        extract(shortcode_atts(array(
            'testimonial_title' => '',
            'testimonial_name' => '',
            'testimonial_para' => '',
            'testimonial_bg' => '',
            'testimonial_picture' => ''
        ), $atts));
        if ($atts['testimonial_picture'] != null) {
            $img = wp_get_attachment_image_src($atts['testimonial_picture']);
            $img = '<img src="' . esc_url($img[0]) . '" alt="" class="img-responsive img-rounded">';
        } else
            $img = '';

        return '<div class="about-widget team-members gardener-list testi-widget clearfix" style="background-color: '.esc_attr($atts['testimonial_bg']).'">
                            <div class="post-media entry">
                               ' . $img . '
                            </div><!-- end media -->
                            <div class="small-title">
                                <small>' . $atts['testimonial_title'] . '</small>
                                <h4>' . $atts['testimonial_name'] . '</h4>
                            </div><!-- end small-title -->
                            <p>' . $atts['testimonial_para'] . '</p>
                        </div>';

    }
}