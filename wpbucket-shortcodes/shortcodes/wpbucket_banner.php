<?php
/**
 * Copyright (c) 01/11/2016.
 * Theme Name: wpbucket-shortcodes
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */
if (!function_exists('wpbucket_banner')) {
    function wpbucket_banner($atts, $content = null)
    {
        return '<h4>' . do_shortcode($content) . '</h4>';
    }
}