<?php
/**
 * Copyright (c) 01/11/2016.
 * Theme Name: wpbucket-shortcodes
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */
if (!function_exists('wpbucket_blog')) {

    function wpbucket_blog($atts, $content = null)
    {
        extract(shortcode_atts(array(
            'wpbucket_choose_post' => ''
        ), $atts));
        if ($atts['wpbucket_choose_post'] != null) {
            $ID = $atts['wpbucket_choose_post'];
            if ( has_post_thumbnail($ID) ) {
                $feat_image_url = wp_get_attachment_url( get_post_thumbnail_id($ID) );
                $feat_image_url = '<img src="'.$feat_image_url.'" alt="" class="img-responsive img-rounded">';
            }else{
                $feat_image_url ='';
            }
            $post_author_id = get_post_field( 'post_author', $ID );
            return '<div class="about-widget blog-wrapper">
                            <div class="post-media entry">
                                '.$feat_image_url.'
                                <div class="magnifier">
                                    <div class="buttons">
                                        <a class="st" rel="bookmark" href="' . get_the_permalink($ID) . '"><span class="fa fa-link"></span></a>
                                    </div><!-- end buttons -->
                                </div><!-- end magnifier -->
                            </div><!-- end media -->
                            <div class="small-title">
                                <small>'.get_the_date(get_option('date_format'),$ID).'</small>
                                <small><a href="'.get_author_posts_url( get_the_author_meta($post_author_id) ).'">'. esc_html__('by','wpbucket').' '.get_the_author_meta('display_name',$post_author_id).'</a></small>
                            <h4><a href = "' . get_the_permalink($ID) . '" > '.get_the_title($ID).' </a></h4>
                            </div><!--end small - title-->
                            <p>'.get_the_excerpt($ID).'</p>
                            <a class="readmore" href = "' . get_the_permalink($ID) . '"> '.esc_html__('Read More','wpbucket').' </a>
                        </div> ';
        } else
            return 'No Post Selected';


    }
}