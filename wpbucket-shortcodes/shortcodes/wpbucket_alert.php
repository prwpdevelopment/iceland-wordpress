<?php
/**
 * Copyright (c) 29/10/2016.
 * Theme Name: wpbucket-shortcodes
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */
if (!function_exists('wpbucket_alert')) {
    function wpbucket_alert($atts, $content = null)
    {
        extract(shortcode_atts(array(
            'alert_title' => '',
            'alert_sub_title' => '',
            'alert_type' => ''
        ), $atts));

        return '<div class="alert ' . esc_attr($atts['alert_type']) . '" role="alert">
                            <strong>' . esc_html($atts['alert_title']) . '</strong> ' . $atts['alert_sub_title'] . '
                        </div>';
    }
}