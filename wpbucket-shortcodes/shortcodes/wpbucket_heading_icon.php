<?php
/**
 * Copyright (c) 05/11/2016.
 * Theme Name: wpbucket-shortcodes
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */

if (!function_exists('wpbucket_heading_icon')) {
    function wpbucket_heading_icon($atts, $content=null){
        extract(shortcode_atts(array(
            'primary_title' => '',
            'secondary_title' => '',
            'heading_icon' => ''
        ), $atts));
        $primary_heading = '';
        $secondary_heading = '';
        if ($atts['primary_title'] != null) {
            $primary_heading .= '<h3>' . $atts['primary_title'] . '</h3>';
        }
        if ($atts['secondary_title'] != null) {
            $secondary_heading .= '<p class="lead">' . $atts['secondary_title'] . '</p>  ';
        }

        return '<div class="section-title">
                    <i class="'.esc_attr($atts['heading_icon']).'"></i>
                    '.$primary_heading.$secondary_heading.'
                </div>';
    }
}