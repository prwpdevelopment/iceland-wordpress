<?php
/**
 * Copyright (c) 31/10/2016.
 * Theme Name: wpbucket-shortcodes
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */
if (!function_exists('contact_base_details')) {
    function contact_base_details($atts, $content=null){
        return '<div class="workinghours"><ul>'.do_shortcode($content).'</ul></div>';
    }
}

if (!function_exists('wpbucket_contact_details')) {

    function wpbucket_contact_details($atts, $content=null){
        extract(shortcode_atts(array(
            'contact_question' => '',
            'contact_answer' => '',
            'contact_icon' => '',
        ), $atts));

        return '<li><i class="fa '.esc_attr($atts['contact_icon']).'"></i> '.esc_html($atts['contact_question']).' <span>'.esc_html($atts['contact_answer']).'</span></li>';
    }
}