<?php
/**
 * Copyright (c) 29/10/2016.
 * Theme Name: wpbucket-shortcodes
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */
if (!function_exists('wpbucket_heading')) {
    function wpbucket_heading($atts, $content = "")
    {
        extract(shortcode_atts(array(
            'primary_title' => '',
            'secondary_title' => ''
        ), $atts));
        $primary_heading = '';
        $secondary_heading = '';
        if ($atts['primary_title'] != null) {
            $primary_heading .= '<h3>' . $atts['primary_title'] . '</h3>';
        }
        if ($atts['secondary_title'] != null) {
            $secondary_heading .= '<p class="lead">' . $atts['secondary_title'] . '</p>  ';
        }
        $html = '<div class="section-title text-center">' . balanceTags($primary_heading) . balanceTags($secondary_heading) . '</div>';
        return $html;
    }
}