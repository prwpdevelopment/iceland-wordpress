<?php
/**
 * Copyright (c) 01/11/2016.
 * Theme Name: wpbucket-shortcodes
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */
if (!function_exists('wpbucket_gardeners')) {

    function wpbucket_gardeners($atts, $content = null)
    {
        extract(shortcode_atts(array(
            'gardener_designation' => '',
            'gardener_name' => '',
            'gardener_para' => '',
            'gardener_picture' => '',
            'gardener_social_icon' => '',
            'gardener_social_url' => ''
        ), $atts));

        if ($atts['gardener_picture'] != null) {
            $img = wp_get_attachment_image_src($atts['gardener_picture'], 'full');
            $img = '<img src="' . esc_url($img[0]) . '" alt="" class="img-responsive img-rounded">';
        } else
            $img = '';

        if ($atts['gardener_social_icon'] != null) {
            $social_html = '<div class="buttons">';
            $social_icon = explode(',', $atts['gardener_social_icon']);
            $social_url = explode(',', $atts['gardener_social_url']);
            $url = '#';

            foreach ($social_icon as $key => $value) {
                if ($social_url[$key] != null || !empty($social_url[$key])) {
                    $url = $social_url[$key];
                }
                $social_html .= ' <a target="_blank" class="st" rel="bookmark" href="' . esc_url($url) . '"><span class="fa ' . esc_attr($value) . '"></span></a>';
            }
            $social_html .= '</div>';
        }

        return '<div class="about-widget team-members">
                            <div class="post-media entry">
                                '.$img.'
                                <div class="magnifier">
                                    '.$social_html.'
                                </div><!-- end magnifier -->
                            </div><!-- end media -->
                            <div class="small-title">
                                <small>'.esc_html($atts['gardener_designation']).'</small>
                                <h4>'.esc_html($atts['gardener_name']).'</h4>
                            </div><!-- end small-title -->
                            <p>'.esc_html($atts['gardener_para']).'</p>
                        </div>';
    }
}