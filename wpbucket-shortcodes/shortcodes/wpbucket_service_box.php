<?php
/**
 * Copyright (c) 31/10/2016.
 * Theme Name: wpbucket-shortcodes
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */
if (!function_exists('wpbucket_service_box')) {
    function wpbucket_service_box($atts,$content=null){
        extract(shortcode_atts(array(
            'service_box_title' => '',
            'service_box_para' => '',
            'service_box_icon' => '',
            'service_box_image' => ''
        ), $atts));
        if ($atts['service_box_image'] != null) {
            $img = wp_get_attachment_image_src($atts['service_box_image'],'full');
            $img = '<img src="' . esc_url($img[0]) . '" alt="" class="img-responsive img-rounded">';
        } else
            $img = '';

        return '<div class="about-widget">
                            <div class="post-media entry">
                                '.$img.'<div class="magnifier"></div>
                            </div><!-- end media -->
                            <div class="small-title">
                                <h4><i class="'.esc_attr($atts['service_box_icon']).'"></i> '.esc_html($atts['service_box_title']).'</h4>
                            </div><!-- end small-title -->
                            <p>'.$atts['service_box_para'].'</p>
                        </div>';
    }
}