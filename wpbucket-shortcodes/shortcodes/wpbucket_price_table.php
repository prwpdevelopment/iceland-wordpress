<?php
/**
 * Copyright (c) 31/10/2016.
 * Theme Name: wpbucket-shortcodes
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */
if (!function_exists('wpbucket_price_table')) {

    function wpbucket_price_table($atts, $content=null){

        extract(shortcode_atts(array(
            'pricetable_title' => '',
            'currency_symbol' => '',
            'amount_price_table' => '',
            'button_text_price_table' => '',
            'button_url_price_table' => '',
            'content_alignment' => '',
            'higilight_price_table' => ''
        ), $atts));
        $href = vc_build_link($atts['button_url_price_table']);
        return '<div class="pricing-box-03 '.esc_attr($atts['higilight_price_table']).' '.esc_attr($atts['content_alignment']).'">
                            <div class="pricing-box-03-head">
                                <h3><sup>'.esc_html($atts['currency_symbol']).'</sup>'.esc_html($atts['amount_price_table']).'</h3>
                                <h4>'.esc_html($atts['pricetable_title']).'</h4>
                            </div>
                            <!-- end pricing-box-09-head -->
                            <div class="pricing-box-03-body">
                               '.preg_replace('#^<\/p>|<p>$#', '', do_shortcode($content)).'
                            </div>
                            <!-- end pricing-box-06-body -->
                            <div class="pricing-box-03-foot">
                                <a href="'.esc_url($href['url']).'" class="btn btn-primary">'.esc_html($atts['button_text_price_table']).'</a>
                            </div>
                            <!-- end pricing-box-03-foot -->
                        </div>';
    }
}