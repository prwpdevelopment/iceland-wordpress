<?php

/**
 * Copyright (c) 01/11/2016.
 * Theme Name: wpbucket-shortcodes
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */
class Wpbucket_vc_posts_field
{
    static function wpbucket_get_posts_field($settings, $value)
    {
        $args = array(
            'posts_per_page'   => -1,
            'offset'           => 0,
            'category'         => '',
            'category_name'    => '',
            'orderby'          => 'date',
            'order'            => 'DESC',
            'include'          => '',
            'exclude'          => '',
            'meta_key'         => '',
            'meta_value'       => '',
            'post_type'        => 'post',
            'post_mime_type'   => '',
            'post_parent'      => '',
            'author'	   => '',
            'author_name'	   => '',
            'post_status'      => 'publish',
            'suppress_filters' => true
        );
        $posts = get_posts($args);
        $posts_html = '<select class="wpbucket_get_post_field"><option vlaue="">---SELECT A POST---</option>';
        foreach ($posts as $post) {
            setup_postdata($post);
            $posts_html .= '<option value="' . $post->ID . '">' . $post->post_title . '</option>';
        }
        wp_reset_postdata();
        $posts_html .= '</select>';
        return '<div class="my_param_block">
                        ' . $posts_html . '<input name="' . esc_attr($settings['param_name']) . '" class="wpb_vc_param_value wpb-textinput ' .
        esc_attr($settings['param_name']) . ' ' .
        esc_attr($settings['type']) . '_field" type="hidden" value="' . esc_attr($value) . '" />'
        . '</div>';
    }
}