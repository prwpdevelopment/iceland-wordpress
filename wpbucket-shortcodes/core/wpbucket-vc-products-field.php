<?php

/**
 * Copyright (c) 01/11/2016.
 * Theme Name: wpbucket-shortcodes
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */
class Wpbucket_vc_products_field
{
    static function wpbucket_get_products_field($settings, $value)
    {
        $args = array(
            'posts_per_page'   => -1,
            'offset'           => 0,
            'category'         => '',
            'category_name'    => '',
            'orderby'          => 'date',
            'order'            => 'DESC',
            'include'          => '',
            'exclude'          => '',
            'meta_key'         => '',
            'meta_value'       => '',
            'post_type'        => 'product',
            'post_mime_type'   => '',
            'post_parent'      => '',
            'author'	   => '',
            'author_name'	   => '',
            'post_status'      => 'publish',
            'suppress_filters' => true
        );
        $products = get_posts($args);
        $products_html = '<select class="wpbucket_get_products_field"><option vlaue="">---SELECT A POST---</option>';
        foreach ($products as $post) {
            setup_postdata($post);
            $products_html .= '<option value="' . $post->ID . '">' . $post->post_title . '</option>';
        }
        wp_reset_postdata();
        $products_html .= '</select>';
        return '<div class="my_param_block">
                        ' . $products_html . '<input name="' . esc_attr($settings['param_name']) . '" class="wpb_vc_param_value wpb-textinput ' .
        esc_attr($settings['param_name']) . ' ' .
        esc_attr($settings['type']) . '_field" type="hidden" value="' . esc_attr($value) . '" />'
        . '</div>';
    }
}