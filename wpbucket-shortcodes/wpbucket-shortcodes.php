<?php

/*
Plugin Name: Wpbucket Shortcodes
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: A brief description of the Plugin.
Version: 1.0
Author: User
Author URI: http://URI_Of_The_Plugin_Author
License: A "Slug" license name e.g. GPL2
*/
register_activation_hook(__FILE__, 'required_plugin_activated');
function required_plugin_activated()
{
    if (!is_plugin_active('js_composer/js_composer.php') and current_user_can('activate_plugins')) {
        // Stop activation redirect and show error
        wp_die('Sorry, but this plugin requires the Visual Composer plugin to be installed and active. <br><a href="' . admin_url('plugins.php') . '">&laquo; Return to Plugins</a>');
    }
}

/*
 * ENQUE FONTAWESOME ICON CSS
 * */
add_action('admin_enqueue_scripts', 'wp_add_icons');
function wp_add_icons()
{
    wp_register_style('wpbucket-fontawesome', plugins_url('wpbucket-shortcodes/css/css/font-awesome.min.css'));
    wp_enqueue_style('wpbucket-fontawesome');
    wp_register_style('wpbucket-flaticon', plugins_url('wpbucket-shortcodes/css/css/flaticon.css'));
    wp_enqueue_style('wpbucket-flaticon');
}

/*
 * VC MAPPING FILE
 * */
include_once plugin_dir_path(__FILE__) . '/vc-maping/wpbucket-vc-mapping.php';
add_action('vc_before_init', 'Wpbucket_vc_mapping::wpbucket_vc_map');
$attributes = array(
    'type' => 'colorpicker',
    'heading' => "Wrapper Background Color",
    'param_name' => 'bg_style',
    'value' => '',
    'description' => __("Set row wrapper background color", "my-text-domain")
);
vc_add_param('vc_row', $attributes); // Note: 'vc_row' was used as a base for "Row" element

/*
 * ADD VISUAL COMPOSER CUSTOM ICON FIELD TYPE.
 *
 * */
include_once plugin_dir_path(__FILE__) . '/core/wpbucket-vc-icon-field.php';
vc_add_shortcode_param('wpbucket_icon', 'Wpbucket_vc_icon_field::wpbucket_icon', plugins_url('wpbucket-shortcodes/core/js/fontawesome.js'));
vc_add_shortcode_param('wpbucket_flat_icon', 'Wpbucket_vc_icon_field::wpbucket_flat_icon', plugins_url('wpbucket-shortcodes/core/js/flatIcon.js'));

/*
 * ADD VISUAL COMPOSER FETCH ALL POSTS
 * */
include_once plugin_dir_path(__FILE__) . '/core/wpbucket-vc-posts-field.php';
vc_add_shortcode_param('wpbucket_get_posts_field', 'Wpbucket_vc_posts_field::wpbucket_get_posts_field', plugins_url('wpbucket-shortcodes/core/js/wpbucket_post.js'));


/*
 * ADD VISUAL COMPOSER FETCH ALL PRODUCTS
 * */
include_once plugin_dir_path(__FILE__) . '/core/wpbucket-vc-products-field.php';
vc_add_shortcode_param('wpbucket_get_products_field', 'Wpbucket_vc_products_field::wpbucket_get_products_field', plugins_url('wpbucket-shortcodes/core/js/wpbucket_products.js'));


/*
 * NESTED SHORT CODE
 * */
if (class_exists('WPBakeryShortCodesContainer')) {
    class WPBakeryShortCode_Contact_Base_Details extends WPBakeryShortCodesContainer
    {
    }
}
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_Wpbucket_Contact_Details extends WPBakeryShortCode
    {
    }
}
if (class_exists('WPBakeryShortCodesContainer')) {
    class WPBakeryShortCode_Wpbucket_Tab_Section_Master extends WPBakeryShortCodesContainer
    {
    }
}
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_Wpbucket_Tab_Section_Child extends WPBakeryShortCode
    {
    }
}
if (class_exists('WPBakeryShortCodesContainer')) {
    class WPBakeryShortCode_Wpbucket_Tab_Content_Master extends WPBakeryShortCodesContainer
    {
    }
}
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_Wpbucket_Tab_Content_Child extends WPBakeryShortCode
    {
    }
}
    /*
     * SHORTCODES FILES
     *
     * */
    include_once plugin_dir_path(__FILE__) . '/shortcodes/wpbucket_heading.php';
    include_once plugin_dir_path(__FILE__) . '/shortcodes/wpbucket_heading_icon.php';
    include_once plugin_dir_path(__FILE__) . '/shortcodes/wpbucket_buttons.php';
    include_once plugin_dir_path(__FILE__) . '/shortcodes/wpbucket_alert.php';
    include_once plugin_dir_path(__FILE__) . '/shortcodes/wpbucket_testimonial.php';
    include_once plugin_dir_path(__FILE__) . '/shortcodes/contact_base_details.php';
    include_once plugin_dir_path(__FILE__) . '/shortcodes/wpbucket_feature_box.php';
    include_once plugin_dir_path(__FILE__) . '/shortcodes/wpbucket_service_box.php';
    include_once plugin_dir_path(__FILE__) . '/shortcodes/wpbucket_price_table.php';
    include_once plugin_dir_path(__FILE__) . '/shortcodes/wpbucket_blog.php';
    include_once plugin_dir_path(__FILE__) . '/shortcodes/wpbucket_banner.php';
    include_once plugin_dir_path(__FILE__) . '/shortcodes/wpbucket_gardeners.php';
    include_once plugin_dir_path(__FILE__) . '/shortcodes/wpbucket_tab_section_master.php';
    include_once plugin_dir_path(__FILE__) . '/shortcodes/wpbucket_tab_content_master.php';
    include_once plugin_dir_path(__FILE__) . '/shortcodes/wpbucket_iceland_content.php';
    include_once plugin_dir_path(__FILE__) . '/shortcodes/wpbucket_product.php';

    add_shortcode('wpbucket_heading', 'wpbucket_heading');
    add_shortcode('wpbucket_heading_icon', 'wpbucket_heading_icon');
    add_shortcode('wpbucket_buttons', 'wpbucket_buttons');
    add_shortcode('wpbucket_alert', 'wpbucket_alert');
    add_shortcode('wpbucket_testimonial', 'wpbucket_testimonial');
    add_shortcode('contact_base_details', 'contact_base_details');
    add_shortcode('wpbucket_contact_details', 'wpbucket_contact_details');
    add_shortcode('wpbucket_feature_box', 'wpbucket_feature_box');
    add_shortcode('wpbucket_service_box', 'wpbucket_service_box');
    add_shortcode('wpbucket_price_table', 'wpbucket_price_table');
    add_shortcode('wpbucket_blog', 'wpbucket_blog');
    add_shortcode('wpbucket_banner', 'wpbucket_banner');
    add_shortcode('wpbucket_gardeners', 'wpbucket_gardeners');
    add_shortcode('wpbucket_tab_section_master', 'wpbucket_tab_section_master');
    add_shortcode('wpbucket_tab_section_child', 'wpbucket_tab_section_child');
    add_shortcode('wpbucket_tab_content_master', 'wpbucket_tab_content_master');
    add_shortcode('wpbucket_tab_content_child', 'wpbucket_tab_content_child');
    add_shortcode('wpbucket_iceland_content', 'wpbucket_iceland_content');
    add_shortcode('wpbucket_product', 'wpbucket_product');