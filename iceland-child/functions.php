<?php

/* ---------------------------------------------------
 * Theme: Restless - Dynamic Content and Layout WordPress Theme
 * Author: Pixel Industry
 * URL: www.pixel-industry.com
  -------------------------------------------------- */

function wpbucket_theme_enqueue_styles()
{

	$parent_style = 'parent-style';
	$header_style = 'header-style';
	$parent_bootstrap = 'bootstrap';
	$parent_bootstrap_theme = 'bootstrap_theme';
	wp_enqueue_style($parent_bootstrap, get_template_directory_uri() . '/css/bootstrap.min.css');
	wp_enqueue_style($parent_style, get_template_directory_uri() . '/style.css', array($parent_bootstrap));
	wp_enqueue_style('child-style',
		get_stylesheet_directory_uri() . '/style.css',
		array($parent_style)
	);
	wp_enqueue_script('child-custom', get_stylesheet_directory_uri() . '/custom.js', array('jquery'), '', true);
}

add_action('wp_enqueue_scripts', 'wpbucket_theme_enqueue_styles');
?>