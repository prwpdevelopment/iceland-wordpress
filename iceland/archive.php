<?php
/**
 * Copyright (c) 24/10/2016.
 * Theme Name: iceland
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */
defined('ABSPATH') or die("No script kiddies please!");

get_header();
get_template_part('menu-section');
get_template_part('page-title-archive');
?>
<div class="section">
    <div class="container">
        <div class="row blog-wrapper blog-grid">
            <?php if (have_posts()) : ?>

                <?php
                // Start the loop.
                while (have_posts()) : the_post();

                    /*
                     * Include the Post-Format-specific template for the content.
                     * If you want to override this in a child theme, then include a file
                     * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                     */
                    get_template_part('template-parts/content', get_post_format());

                    // End the loop.
                endwhile;

            // If no content, include the "No posts found" template.
            else :
                get_template_part('template-parts/content', 'none');

            endif;
            ?>
        </div>
        <div class="clearfix"></div>
        <div class="pagination-wrap text-center">

            <?php echo Wpbucket_Partials::pagination('blog'); ?>

        </div>
    </div>
</div>
<?php
get_footer();
?>
