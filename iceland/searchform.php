<?php
/**
 * Copyright (c) 06/11/2016.
 * Theme Name: iceland
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */
?>

<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label>
		<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label', 'wpbucket' ); ?></span>
		<input type="search" class="search-field form-control" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'wpbucket' ); ?>" value="<?php echo get_search_query(); ?>" name="s" onkeydown="if (event.keyCode == 13) {
                                this.form.submit();
                                return false;
                            }"/>
	</label>
</form>
