<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

?>
<div class="account_page">
    <div class="row">
        <?php wc_print_notices(); ?>

        <?php do_action('woocommerce_before_customer_login_form'); ?>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 login_form">
            <?php if (get_option('woocommerce_enable_myaccount_registration') === 'yes') : ?>

            <?php endif; ?>

            <div class="widget-title">
                <h3><?php _e('Login Account', 'wpbucket'); ?></h3>
                <hr>
            </div>
            <form method="post" class="login">

                <?php do_action('woocommerce_login_form_start'); ?>

                <div class="form_group">
                    <label for="username"><?php _e('Username or email address', 'wpbucket'); ?> <span
                            class="required">*</span></label>
                    <div class="input_group">
                        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username"
                               id="username"
                               value="<?php if (!empty($_POST['username'])) echo esc_attr($_POST['username']); ?>"/>
                        <i class="fa fa-user" aria-hidden="true"></i>
                    </div>
                </div>
                <div class="form_group">
                    <label for="password"><?php _e('Password', 'wpbucket'); ?> <span
                            class="required">*</span></label>
                    <div class="input_group">
                        <input class="woocommerce-Input woocommerce-Input--text input-text" type="password"
                               name="password"
                               id="password" placeholder="********"/>
                        <i class="fa fa-lock" aria-hidden="true"></i>
                    </div>
                </div>

                <?php do_action('woocommerce_login_form'); ?>
                <div class="clear_fix">
                    <div class="float_left">
                        <input class="woocommerce-Input woocommerce-Input--checkbox" name="rememberme" type="checkbox"
                               id="rememberme" value="forever"/>
                        <label for="remember"><?php _e('Remember me', 'wpbucket'); ?></label>
                    </div>
                    <!-- End .single_checkbox -->
                </div>
                <div class="form_group">
                    <?php wp_nonce_field('woocommerce-login', 'woocommerce-login-nonce'); ?>
                    <input type="submit" class="btn btn-primary" name="login"
                           value="<?php esc_attr_e('Login Now', 'wpbucket'); ?>"/>
                </div>
                <div class="form_group">
                    <a href="<?php echo esc_url(wp_lostpassword_url()); ?>"><?php _e('Lost your password?', 'wpbucket'); ?></a>
                </div>

                <?php do_action('woocommerce_login_form_end'); ?>

            </form>

            <?php if (get_option('woocommerce_enable_myaccount_registration') === 'yes') : ?>

        </div>

        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 register_form m30">

            <div class="widget-title">
                <h3><?php _e('Register Account', 'wpbucket'); ?></h3>
                <hr>
            </div>
            <form method="post" class="register">

                <?php do_action('woocommerce_register_form_start'); ?>

                <?php if ('no' === get_option('woocommerce_registration_generate_username')) : ?>

                    <div class="form_group">
                        <label for="reg_username"><?php _e('Username', 'wpbucket'); ?> <span
                                class="required">*</span></label>
                        <div class="input_group">
                            <input type="text" class="woocommerce-Input woocommerce-Input--text input-text"
                                   name="username"
                                   id="reg_username"
                                   value="<?php if (!empty($_POST['username'])) echo esc_attr($_POST['username']); ?>"/>
                            <i class="fa fa-user" aria-hidden="true"></i>
                        </div>
                    </div>

                <?php endif; ?>

                <div class="form_group">
                    <label for="reg_email"><?php _e('Email address', 'wpbucket'); ?> <span
                            class="required">*</span></label>
                    <div class="input_group">
                        <input type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="email"
                               id="reg_email"
                               value="<?php if (!empty($_POST['email'])) echo esc_attr($_POST['email']); ?>"/>
                        <i class="fa fa-user" aria-hidden="true"></i>
                    </div>
                </div>

                <?php if ('no' === get_option('woocommerce_registration_generate_password')) : ?>

                    <div class="form_group">
                        <label for="reg_password"><?php _e('Password', 'wpbucket'); ?> <span
                                class="required">*</span></label>
                        <div class="input_group">
                            <input type="password" class="woocommerce-Input woocommerce-Input--text input-text"
                                   name="password"
                                   id="reg_password"/>
                            <i class="fa fa-lock" aria-hidden="true"></i>
                        </div>
                    </div>

                <?php endif; ?>

                <!-- Spam Trap -->
                <div style="<?php echo((is_rtl()) ? 'right' : 'left'); ?>: -999em; position: absolute;"><label
                        for="trap"><?php _e('Anti-spam', 'wpbucket'); ?></label><input type="text" name="email_2"
                                                                                          id="trap" tabindex="-1"
                                                                                          autocomplete="off"/></div>

                <?php do_action('woocommerce_register_form'); ?>
                <?php do_action('register_form'); ?>

                <div class="form_group">
                    <?php wp_nonce_field('woocommerce-register', 'woocommerce-register-nonce'); ?>
                    <input type="submit" class="btn btn-primary st" name="register"
                           value="<?php esc_attr_e('Create Account', 'wpbucket'); ?>"/>
                </div>

                <?php do_action('woocommerce_register_form_end'); ?>

            </form>

        </div>
    <?php endif; ?>
    </div>
</div>
<?php do_action('woocommerce_after_customer_login_form'); ?>
