<?php
/**
 * Copyright (c) 26/10/2016.
 * Theme Name: iceland
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */
?>

<?php if (WPBUCKET_META_BOX) {

    $page_title_image = rwmb_meta('pg_title_image', 'type=image&size=full', get_queried_object_id());

    $parallax = 'parallax-off';
    if (rwmb_meta('pg_parallax_on_off', '', get_queried_object_id()) == 1) {
        $parallax = 'parallax';
    }

    if (!empty($page_title_image)) {
        foreach ($page_title_image as $value) {
            $url = $value['url'];
        }
        $page_title_style = "style='background-image: url(" . esc_url_raw($url) . ") !important; background-size: cover;'";
    } else {
        $page_title_style = "style='background-image: url(" . (WPBUCKET_TEMPLATEURL) . '/uploads/page_parallax_02.jpg' . ") !important;'";
    }
    ?>
    <div class="page-parallax <?php echo esc_attr($parallax) ?>"
         data-stellar-background-ratio="1" <?php echo($page_title_style); ?>>
    </div>
    <?php if (rwmb_meta('pg_hide_title', '', get_queried_object_id()) == 0) {
        $main_title = rwmb_meta('pg_page_main_title', '', get_queried_object_id());
        $extra_title = rwmb_meta('pg_page_sub_title', '', get_queried_object_id());
        $main_title_html = get_the_title(get_the_ID());
        $extra_title_html = '';

        if ($main_title != '') {
            $main_title_html = $main_title;
        }
        if ($extra_title != '') {
            $extra_title_html = $extra_title;
        }
        if (is_post_type_archive() && WPBUCKET_WOOCOMMERCE) { ?>
            <div class="page-title">
                <div class="container">
                    <div class="section-title">
                        <h3><?php woocommerce_page_title(); ?></h3>
                        <p class="lead"><?php do_action( 'woocommerce_archive_description' ); ?></p>
                    </div><!-- end title -->
                </div><!-- end container -->
            </div>
        <?php } else {
            ?>
            <div class="page-title">
                <div class="container">
                    <div class="section-title">
                        <h3><?php echo esc_html($main_title_html); ?></h3>
                        <p class="lead"><?php echo esc_html($extra_title_html); ?></p>
                    </div><!-- end title -->
                </div><!-- end container -->
            </div>
        <?php } ?>
        <!-- end section -->
    <?php } else {
    } ?>
<?php } else { ?>
    <div class="page-parallax parallax parallax-off" data-stellar-background-ratio="1"
         style="background-image:url('<?php echo WPBUCKET_TEMPLATEURL; ?>/uploads/page_parallax_02.jpg');">

    </div>
    <div class="page-title">
        <div class="container">
            <div class="section-title">
                <h3><?php echo get_the_title(get_the_ID()); ?></h3>
            </div><!-- end title -->
        </div><!-- end container -->
    </div>
<?php } ?>