/*------------------------------------------------------------------
    File Name: custom.js
    Template Name: Iceland
    Created By: Similar Icons
    Envato Profile: http://themeforest.net/user/similaricons
    Website: https://similaricons.com
    Version: 1.0
-------------------------------------------------------------------*/

/*------------------------------------------------------------------
    ADD YOUR CUSTOM JS HERE
-------------------------------------------------------------------*/
(function($) {
    "use strict";

    /*!
     * PRELOAD
     */
    $(window).load(function() {
        $("#preloader").on(500).fadeOut();
        $(".preloader").on(600).fadeOut("slow");
    });

    /*
    * REMOVE ALL P EMPTY TAG
    * */
    $('p').each(function() {
        var $this = $(this);
        if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
            $this.remove();
    });
    /*!
     * PANEL
     */
    $(document).on('click', 'span.clickable', function(e) {
        var $this = $(this);
        if (!$this.hasClass('panel-collapsed')) {
            $this.parents('.panel').find('.panel-body').slideUp();
            $this.addClass('panel-collapsed');
            $this.find('i').removeClass('fa-angle-up').addClass('fa-angle-down');
        } else {
            $this.parents('.panel').find('.panel-body').slideDown();
            $this.removeClass('panel-collapsed');
            $this.find('i').removeClass('fa-angle-down').addClass('fa-angle-up');
        }
    })

    /*!
     * TOOLTIP
     */
    jQuery('[data-toggle="tooltip"]').tooltip()

    /*!
     * CONTACT
     */
    jQuery(document).ready(function() {
        $('#contactform').submit(function() {
            var action = $(this).attr('action');
            $("#message").slideUp(750, function() {
                $('#message').hide();
                $('#submit')
                    .after('<img src="images/ajax-loader.gif" class="loader" />')
                    .attr('disabled', 'disabled');
                $.post(action, {
                        first_name: $('#first_name').val(),
                        appo_time: $('#appo_time').val(),
                        email: $('#email').val(),
                        phone: $('#phone').val(),
                        select_gardener: $('#select_gardener').val(),
                        select_service: $('#select_service').val(),
                        comments: $('#comments').val(),
                        verify: $('#verify').val()
                    },
                    function(data) {
                        document.getElementById('message').innerHTML = data;
                        $('#message').slideDown('slow');
                        $('#contactform img.loader').fadeOut('slow', function() {
                            $(this).remove()
                        });
                        $('#submit').removeAttr('disabled');
                        if (data.match('success') != null) $('#contactform').slideUp('slow');
                    }
                );
            });
            return false;
        });
    });
})(jQuery);