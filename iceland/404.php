<?php
/**
 * Copyright (c) 24/10/2016.
 * Theme Name: iceland
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */

defined('ABSPATH') or die("No script kiddies please!");

get_header();
get_template_part('menu-section');

$main_title = wpbucket_return_theme_option('404_main_heading','','404');
$sub_title = wpbucket_return_theme_option('404_sub_heading','','Page Not Found');
$description = wpbucket_return_theme_option('404_description','','The page you are looking for no longer exists. Perhaps you can return back to the site\'s homepage and see if you can find what you are looking for');

?>
    <div class="section">
        <div class="container">
            <div class="row pagenot wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">
                <div class="col-md-10 col-md-offset-1 text-center">
                    <h2><?php echo esc_html($main_title);?></h2>
                    <h1><?php echo esc_html($sub_title);?></h1>
                    <p class="lead"><?php echo balanceTags($description);?></p>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- end container -->
    </div>

<?php
get_footer();
?>