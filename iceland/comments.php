<?php
/**
 * Copyright (c) 24/10/2016.
 * Theme Name: iceland
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */

if (post_password_required()) : ?>
    <p class="nopassword"><?php esc_html_e('This post is password protected. Enter the password to view any comments.', 'wpbucket'); ?></p>

    <?php
    /*
     * Stop the rest of comments.php from being processed,
     * but don't kill the script entirely -- we still have
     * to fully load the template.
     */
    return;


endif;
?>
<?php if (have_comments()) : ?>
    <div class="content-widget clearfix">

        <div class="widget-title">
            <h3><?php echo Wpbucket_Helpers::wpbucket_get_comments_number(get_the_ID(), true); ?></h3>
            <hr>
        </div>

        <!-- post comments list items start -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-body comments">
                        <ul class="comment-list">
                            <?php
                            /*
                             * Loop through and list the comments.
                             */
                            wp_list_comments(array(
                                'callback' => 'Wpbucket_partials::wpbucket_render_comments'
                            ));
                            ?>
                            <?php if (get_comment_pages_count() > 1 && get_option('page_comments')): ?>
                                <li class="comments-pagination">
                                    <?php paginate_comments_links(); ?>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- post comments list items end -->
    </div>


    <?php
elseif (!comments_open() && !is_page() && post_type_supports(get_post_type(), 'comments')) :
    ?>
    <div class="col-md-12">
        <p class="nocomments"><?php esc_html_e('Comments are closed.', 'wpbucket'); ?></p>
    </div>
    <?php
endif;
?>

<?php if (comments_open()) : ?>
    <div class="content-widget clearfix">
        <div id="respond">
            <div class="widget-title">
                <h3><?php comment_form_title('Comment Form', 'Submit Your Comment To %s'); ?></h3>
                <hr>
            </div>
            <div class="cancel-comment-reply reply">
                <?php cancel_comment_reply_link(); ?>
            </div>
            <div class="form-contact appointment_form comment-form">

                <?php if (get_option('comment_registration') && !is_user_logged_in()) : ?>
                    <div class="col-md-12 col-sm-12">
                        <p>
                            <?php esc_html_e('You must be', 'wpbucket') ?> <a
                                href="<?php echo esc_url(wp_login_url(get_permalink())); ?>"><?php esc_html_e('logged in', 'wpbucket') ?></a> <?php esc_html_e('to post a comment', 'wpbucket') ?>
                            .
                        </p>
                    </div>
                <?php else : ?>
                    <div class="replyForm">
                        <form
                            action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php"
                            method="post" name="comments-form" class="row">
                            <div class="inputColumns clearfix">
                                <?php if (is_user_logged_in()) : ?>
                                    <div class="col-md-12 col-sm-12">
                                        <p class="comment-notes">
                                            <?php esc_html_e('Logged in as', 'wpbucket') ?> <a
                                                href="<?php echo esc_url(get_option('siteurl')); ?>/wp-admin/profile.php"><?php echo esc_html($user_identity); ?>
                                            </a>. <a href="<?php echo esc_url(wp_logout_url(get_permalink())); ?>"
                                                     title="<?php esc_html_e('Log out from this account', 'wpbucket'); ?>"><?php esc_html_e('Logged in as', 'wpbucket') ?> &raquo;</a>
                                        </p>
                                    </div>
                                <?php else : ?>
                                    <div class="col-md-12 col-sm-12">
                                        <p class="comment-notes">
                                            <?php esc_html_e('Your email address will not be published. Required fields are marked', 'wpbucket') ?>
                                            <span class="required">*</span>
                                        </p>
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                        <label><?php esc_html_e('Name', 'wpbucket') ?> <span
                                                class="required">*</span></label>
                                        <input type="text" class="form-control" placeholder="" name="author">
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                        <label><?php esc_html_e('Email', 'wpbucket') ?> <span class="required">*</span></label>
                                        <input type="email" class="form-control" placeholder="" name="email">
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                        <label><?php esc_html_e('Website', 'wpbucket') ?></label>
                                        <input type="text" class="form-control" placeholder="" name="website">
                                    </div>
                                <?php endif; ?>
                            </div>
                            <!--<p>You can use these tags: <code><?php echo allowed_tags(); ?></code></p>-->
                            <div class="col-md-12 col-sm-12">
                                <label><?php esc_html_e('Comment', 'wpbucket') ?> <span class="required">*</span></label>
                                <textarea class="form-control" placeholder="" name="comment"></textarea>
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <input type="submit" value="Send Comment" class="btn btn-primary"/>
                            </div>
                            <?php comment_id_fields(); ?>

                            <?php do_action('comment_form', $post->ID); ?>
                        </form>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="clearfix"></div>
