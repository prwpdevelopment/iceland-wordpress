<?php
/**
 * Copyright (c) 26/10/2016.
 * Theme Name: iceland
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */
defined('ABSPATH') or die("No script kiddies please!");

get_header();
get_template_part('menu-section');
get_template_part('page-title-archive');
?>
<div class="section">
    <div class="container">
        <div class="row blog-wrapper blog-grid">
            <div class="col-md-12">
                <?php
                $author_bio_avatar_size = apply_filters('wpbucket_author_bio_avatar_size', 42);
                echo get_avatar(get_the_author_meta('user_email'), $author_bio_avatar_size);
                ?>
                <div class="author-description">
                    <h2 class="author-title"><span
                            class="author-heading"><?php _e('Author:', 'wpbucket'); ?></span> <?php echo get_the_author_meta('display_name'); ?>
                    </h2>
                    <h4 class="author-title"><span
                            class="author-heading"><?php _e('Email:', 'wpbucket'); ?></span> <?php echo get_the_author_meta('email'); ?>
                    </h4>
                    <h4 class="author-title"><span
                            class="author-heading"><?php _e('Name:', 'wpbucket'); ?></span> <?php echo get_the_author_meta('first_name').' '.get_the_author_meta('last_name');; ?>
                    </h4>
                    <p class="author-bio">
                        <?php the_author_meta('description'); ?>
                    </p><!-- .author-bio -->

                    <h2>
                        <?php echo esc_html__('All posts by author:','wpbucket'); ?>
                    </h2>
                </div>
            </div>
        </div>
        <div class="row blog-wrapper blog-grid">
            <?php if (have_posts()) : ?>

                <?php
                // Start the loop.
                while (have_posts()) : the_post();

                    /*
                     * Include the Post-Format-specific template for the content.
                     * If you want to override this in a child theme, then include a file
                     * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                     */
                    get_template_part('template-parts/content', get_post_format());

                    // End the loop.
                endwhile;

            // If no content, include the "No posts found" template.
            else :
                get_template_part('template-parts/content', 'none');

            endif;
            ?>
        </div>
        <div class="clearfix"></div>
        <div class="pagination-wrap text-center">
            <?php echo Wpbucket_Partials::pagination('blog'); ?>
        </div>
    </div>
</div>
<?php
get_footer();
?>
