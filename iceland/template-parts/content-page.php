<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="single-blog">
        <div class="about-widget">
            <?php if (has_post_thumbnail()){?>
            <div class="post-media entry">
                <img src="<?php echo Wpbucket_Helpers::wpbucket_get_image_url(get_the_ID()) ?>" alt=""
                     class="img-responsive img-rounded">
            </div><!-- end media -->
            <?php } ?>
            <?php
            the_content();
            wp_link_pages(array(
                'before' => '<div class="page-links"><span class="page-links-title">' . __('Pages:', 'wpbucket') . '</span>',
                'after' => '</div>',
                'link_before' => '<span>',
                'link_after' => '</span>',
                'pagelink' => '<span class="screen-reader-text">' . __('Page', 'wpbucket') . ' </span>%',
                'separator' => '<span class="screen-reader-text">, </span>',
            ));
            ?>
        </div><!-- end about-widget -->
        <?php
        edit_post_link(
            sprintf(
            /* translators: %s: Name of current post */
                __('Edit<span class="screen-reader-text"> "%s"</span>', 'wpbucket'),
                get_the_title()
            ),
            '<footer class="entry-footer"><span class="edit-link">',
            '</span></footer><!-- .entry-footer -->'
        );
        ?>
    </div><!-- end col -->
</article><!-- #post-## -->
