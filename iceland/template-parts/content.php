<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="col-md-4 wow fadeIn">
        <div class="about-widget">
            <div class="post-media entry">
                <img src="<?php echo Wpbucket_Helpers::wpbucket_get_image_url(get_the_ID())?>" alt="" class="img-responsive img-rounded">
                <div class="magnifier">
                    <div class="buttons">
                        <a class="st" rel="bookmark" href="<?php the_permalink()?>"><span class="fa fa-link"></span></a>
                    </div><!-- end buttons -->
                </div><!-- end magnifier -->
            </div><!-- end media -->
            <div class="small-title">
                <small><?php echo get_the_date();?></small>
                <small><a href="<?php  echo get_author_posts_url( get_the_author_meta('ID') ); ?>"> <?php echo esc_html__('by','wpbucket').' '.get_the_author_meta('display_name')?></a></small>
                <h4><a href="<?php the_permalink()?>"><?php the_title()?></a></h4>
            </div><!-- end small-title -->
            <?php

            if (is_single()) {
                the_content();
                wp_link_pages(array('before' => '<p class="wp-link-pages">Pages: ', 'after' => '</p>'));
            } else {
                // if user inserts more tag get regular content instead excerpt
                if (preg_match('/<!--more(.*?)?-->/', $post->post_content)) {
                    the_content();
                } else {
                    the_excerpt();
                }
            }
            wp_link_pages(array(
                'before' => '<div class="page-links"><span class="page-links-title">' . __('Pages:', 'wpbucket') . '</span>',
                'after' => '</div>',
                'link_before' => '<span>',
                'link_after' => '</span>',
                'pagelink' => '<span class="screen-reader-text">' . __('Page', 'wpbucket') . ' </span>%',
                'separator' => '<span class="screen-reader-text">, </span>',
            ));
            ?>
            <a class="readmore" href="<?php the_permalink()?>"><?php echo esc_html__('Read More','wpbucket')?></a>
        </div><!-- end about-widget -->
        <?php
        edit_post_link(
            sprintf(
            /* translators: %s: Name of current post */
                __( 'Edit<span class="screen-reader-text"> "%s"</span>', 'wpbucket' ),
                get_the_title()
            ),
            '<footer class="entry-footer"><span class="edit-link">',
            '</span></footer><!-- .entry-footer -->'
        );
        ?>
    </div><!-- end col -->
</article><!-- #post-## -->
