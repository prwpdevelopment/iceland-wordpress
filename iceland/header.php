<?php
/**
 * Copyright (c) 24/10/2016.
 * Theme Name: iceland
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */

defined('ABSPATH') or die("No script kiddies please!");
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta name="description"
          content="<?php
          if (is_single()) {
              single_post_title('', true);
          } else {
              bloginfo('name');
              echo " - ";
              bloginfo('description');
          }
          ?>"/>

    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <?php
    wp_head();
    ?>
</head>
<body <?php body_class(); ?>>

<!-- LOADER -->
<div id="preloader">
    <img class="preloader" src="<?php echo WPBUCKET_TEMPLATEURL; ?>/images/loader.gif" alt="">
</div><!-- end loader -->
<!-- END LOADER -->
<div id="wrapper">