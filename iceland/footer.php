<?php
/**
 * Copyright (c) 24/10/2016.
 * Theme Name: iceland
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */
global $wpbucket_theme_config;
$footer_show_hide = wpbucket_return_theme_option('footer_show', '', 1);

if ($footer_show_hide == 1) {
    ?>
    <footer class="footer">
        <div class="container">
            <div class="row">
                <?php
                $widget_areas = apply_filters('wpbucket_footer_widget_areas', wpbucket_return_theme_option('footer_widget_areas', '', '4'));

                for ($i = 1; $i <= $widget_areas; $i++) {
                    $grid_size = 12 / $widget_areas;
                    ?>
                    <div class="col-md-<?php echo esc_html($grid_size) ?>">

                        <?php
                        $sidebarid = 'wpbucket-sidebar-' . ($i + 1);
                        if (!dynamic_sidebar($sidebarid)) :
                            ?>
                            <div class="widget">
                                <div class="title">
                                    <h3><?php esc_html_e('Widget', 'wpbucket') ?></h3>
                                </div>
                                <p><?php esc_html_e('This is widget area. Set widget here.', 'wpbucket') ?></p>
                            </div>


                            <?php
                        endif;
                        ?>

                    </div>

                <?php } ?>
            </div><!-- end row -->
        </div><!-- end container -->
    </footer><!-- end footer -->

    <div class="copyrights">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <p><?php echo wp_kses( wpbucket_return_theme_option( 'copyright_text' ), $wpbucket_theme_config['allowed_html_tags'] ) ?></p>
                </div><!-- end col -->

                <div class="col-md-6 text-right">
                    <?php
                    echo Wpbucket_Partials::generate_footer_menu_html();
                    ?>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end copyrights -->
<?php } ?>
</div><!-- end wrapper -->
<div class="clear"></div>
<?php wp_footer(); ?>
</body>
</html>