<?php
/**
 * Copyright (c) 24/10/2016.
 * Theme Name: iceland
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */
define( 'THEME_NAME', "Iceland" );
define( 'WPBUCKET_TEMPLATEURL', get_template_directory_uri() );
define( 'WPBUCKET_THEME_DIR', get_template_directory() );
global $wpbucket_theme_config;
/* --------------------------------------------------------------------
 * Array with configurations for current theme
 * -------------------------------------------------------------------- */

$wpbucket_theme_config = array(
    'allowed_html_tags' => array(
        'a' => array(
            'href' => array(),
            'title' => array(),
            'class' => array(),
            'target' => array()
        ),
        'br' => array(),
        'em' => array(),
        'strong' => array(),
        'span' => array( 'class' => array() )
    ),
    'menu_caret' => '0',
    'text_domain' => 'wpbucket',
    'default_color' => 'colors'
);

$wpbucket_theme_config = apply_filters( 'wpbucket_theme_config', $wpbucket_theme_config );
function config_files_dir() {
    return trailingslashit( WPBUCKET_THEME_DIR ) . 'includes/configuration';
}

add_filter( 'wpbucket_config_files_dir', 'config_files_dir' );

require_once WPBUCKET_THEME_DIR . '/core/core-includes.php';
include_once WPBUCKET_THEME_DIR . '/includes/wpbucket-partials.php';
include_once WPBUCKET_THEME_DIR . '/includes/wpbucket-actions.php';
include_once WPBUCKET_THEME_DIR . '/includes/wpbucket-filters.php';