<?php

/*
 * ---------------------------------------------------------
 * Partials
 *
 * Class for functions that return text / HTML content
 * ----------------------------------------------------------
 */

class Wpbucket_Partials
{

    /**
     * Echo logo image HTML
     *
     * @global bool $vlc_is_retina
     * @param array $args
     * @return string
     */
    static function get_logo_image_html($args = array())
    {
        global $vlc_is_retina;

        $website_name = get_bloginfo('name');
        $logo_width = '';
        $styles = '';

        $logo = wpbucket_return_theme_option('logo', 'url');
        $retina_logo = wpbucket_return_theme_option('retina_logo', 'url');

        if ($vlc_is_retina && !empty($retina_logo)) {
            $logo = $retina_logo;
            $logo_width = wpbucket_return_theme_option('logo', 'width');
        } else if (!empty($logo)) {
            $logo_width = wpbucket_return_theme_option('logo', 'width');
        } else {
            $logo = WPBUCKET_TEMPLATEURL . "/img/logo.png";
            $logo_width = "138";
        }

        if (!empty($args) && $args ['float']) {
            $float = $args ['float'];
            $styles = "style='float: {$float};'";
        }

        $logo = esc_url_raw($logo);
        $logo_img = "<img src='{$logo}' alt='{$website_name}' width='{$logo_width}' {$styles}/>";

        return $logo_img;
    }

    /**
     * Returns HTML for logo
     *
     * @return string
     */
    static function generate_logo_html()
    {
        ob_start();
        ?>
        <!-- .logo start -->
        <div class="logo">
            <a href="<?php echo esc_url(home_url('/')); ?>">
                <?php
                echo static::get_logo_image_html();
                ?>
            </a>
        </div>
        <!-- logo end -->
        <?php
        return ob_get_clean();
    }

    /**
     * Blog content search form
     *
     * @return string
     */
    static function get_content_serch_form()
    {
        $form = '<form action="' . esc_url(home_url('/')) . '" method="get" id="header_form">
                     <input class="a_search" name="s" type="text" placeholder="Blog search..." />
                     <input class="search-submit" type="submit" />
                 </form>';

        return $form;
    }

    /**
     * Renders main menu in header.
     *
     * @return type string
     */
    static function generate_menu_html()
    {
        ob_start();

        wp_nav_menu(array(
            'theme_location' => apply_filters('wpbucket_main_menu_location', 'primary'),
            'container' => true,
            'items_wrap' => '<ul id="%1$s" class="nav navbar-nav %2$s">%3$s</ul>',
            'walker' => new Wpbucket_Theme_Menu_Walker (),
            'fallback_cb' => false
        ));

        return ob_get_clean();
    }

    /**
     * Renders footer menu in header.
     *
     * @return type string
     */
    static function generate_footer_menu_html()
    {
        ob_start();
        wp_nav_menu(array(
            'theme_location' => 'footer',
            'container' => false,
            'items_wrap' => '<ul id="%1$s" class="%2$s list-inline">%3$s <li class="dmtop"><a href="#"><i class="fa fa-angle-up"></i></a></li></ul>',
            'walker' => new Wpbucket_Theme_Footer_Menu_Walker (),
            'fallback_cb' => false,
            'responsive_menu' => '1'
        )); // argument added to distinguish regular menu with responsive
        return ob_get_clean();
    }

    /**
     * Paginate function for Blog and Portfolio
     *
     * @global object $wp_query
     * @global object $wp_rewrite
     * @param string $location
     */
    static function pagination($location)
    {
        global $wp_query, $wp_rewrite;

        $pages = '';
        $pagination = '';
        $max = $wp_query->max_num_pages;

        // if variable paged isn't set
        if (!$current = get_query_var('paged'))
            $current = 1;

        // set parameters
        $args = array(
            'base' => str_replace(999999999, '%#%', get_pagenum_link(999999999)),
            'format' => '',
            'total' => $max,
            'current' => $current,
            'show_all' => true,
            'type' => 'array',
            'prev_text' => '&larr;',
            'next_text' => '&rarr;',
            'prev_next' => false,
            'mid_size' => 3,
            'end_size' => 1
        );

        if ($location == 'portfolio') {
            $args ['base'] = @add_query_arg('page', '%#%');

            // check if permalinks are used
            if ($wp_rewrite->using_permalinks())
                $args ['base'] = user_trailingslashit(trailingslashit(remove_query_arg('s', get_pagenum_link(1))) . 'page/%#%/', 'paged');
            if (!empty($wp_query->query_vars ['s']))
                $args ['add_args'] = array(
                    's' => get_query_var('s')
                );
        }

        $previous_label = esc_html__('&#60; Previous', 'wpbucket');
        $next_label = esc_html__('Next &#62;', 'wpbucket');

        // previous and next links html
        $prev_page_link = $current == 1 ? "" : "<li><a class='fa fa-angle-double-left' href='" . get_pagenum_link($current - 1) . "'></a></li>";
        $next_page_link = $current == $max ? "" : "<li><a class='fa fa-angle-double-right' href='" . get_pagenum_link($current + 1) . "'></a></li>";

        // get page link
        $pagination_links = paginate_links($args);

        // loop through pages
        if (!empty($pagination_links)) {
            foreach ($pagination_links as $index => $link) {
                $link = str_replace('</span>', '</a>', $link);
                $link = str_replace('<span', '<a', $link);

                $pagination .= "<li " . (($index + 1) == $current ? "class='active'>" : ">") . $link . "</li>";
            }
        }

        // if there is more then one page send html to browser
        if ($max > 1) {
            if ($location == 'portfolio' || $location == 'blog_timeline') {
                $container = 'div';
            } else {
                $container = 'ul';
            }

            $pagination_html = "<{$container} class='pagination clearfix'>
                        
                        {$prev_page_link}
                        {$pagination}
                        {$next_page_link}
                        
                      </{$container}>";

            if ($location == 'portfolio') {
                echo "<div class='row'><div class='col-md-12'>" . $pagination_html . "</div></div>";
            } else {
                echo balanceTags($pagination_html);
            }
        }
    }

    /**
     * Echo post author html
     *
     */
    static function post_author_html()
    {

        if (wpbucket_return_theme_option('blog_single_show_author') == '1') :
            $about_the_author = wpbucket_return_theme_option('blog_single_author_section_title');
            $avatar_size = 90;
            $post_author_id = get_the_author_meta('ID');
            $post_author_bio = get_the_author_meta('description');
            $post_author_name = get_the_author_meta('display_name');
            $post_author_website = get_the_author_meta('user_url');
            ?>
            <!-- .post-author start -->
            <article class="blog-single-author">
                <div class="avatar-container">
                    <?php echo get_avatar($post_author_id, $avatar_size); ?>
                </div>

                <div class="text-container">
                    <h4><?php echo esc_html($post_author_name) ?></h4>

                    <p><?php echo esc_html($post_author_bio) ?> <a
                            href="<?php echo esc_url_raw($post_author_website) ?>"><strong> <?php echo esc_url($post_author_website) ?></strong></a>
                    </p>
                </div>
            </article>
            <!-- post-author end -->

            <?php
        endif;
    }

    /**
     * Template for comments and pingbacks.
     *
     * @param object $comment
     * @param array $args
     * @param int $depth
     */
    static function wpbucket_render_comments($comment, $args, $depth)
    {
        $GLOBALS ['comment'] = $comment;
        switch ($comment->comment_type) {
            case 'pingback' :
            case 'trackback' :
                ?>
                <li class="post pingback">
                <p>
                    <?php esc_html_e('Pingback', 'wpbucket'); ?><?php comment_author_link(); ?><?php edit_comment_link(esc_html__('Edit', 'wpbucket'), '<span class="edit-link">', '</span>'); ?>
                </p>
                <?php
                break;
            default :
                ?>

            <li id="li-comment-<?php comment_ID(); ?>" class="media">
                <div id="comment-<?php comment_ID(); ?>" class="comment">
                    <a href="#" class="pull-left img-circle">
                        <?php
                        $avatar_size = 80;
                        echo get_avatar($comment, $avatar_size, false, get_comment_author());
                        ?>
                    </a>
                    <div class="media-body">
                        <strong class="text-success"><?php comment_author(); ?></strong>
                            <span class="text-muted"><small class="text-muted">
                                    <?php
                                    echo get_comment_date() . " at ";
                                    comment_time();
                                    ?></small></span>
                        <p>
                            <?php
                            comment_text();
                            ?>
                        </p>
                        <?php comment_reply_link(array_merge($args, array('reply_text' => esc_html__('Reply', 'wpbucket'), 'depth' => $depth, 'max_depth' => $args['max_depth']))); ?>

                    </div>
                </div> <!-- #comment-## -->
                <?php
                break;
        }
    }

    /**
     * Template for categories dropdown.
     */
    static function get_categories_dropdown()
    {
        $categories = get_categories(array(
            'hide_empty' => false
        ));

        $html = '<ul class="dropdown-menu" aria-labelledby="categoriesDropdown"><li class="widget widget_categories"><ul>';
        foreach ($categories as $category) {
            $html .= '<li><a href="' . esc_url(get_category_link($category->term_id)) . '">' . $category->name . '</a></li>';
        }
        $html .= '</ul></li></ul>';

        return balanceTags($html);
    }

    /**
     * Template for categories lists.
     *
     * @param $post_id
     * @param $no_icon for fontawesome control
     * @param $taxonomy for custom taxonomy
     */
    static function get_categories_lists($post_id, $no_icon = NULL, $taxonomy = null)
    {
        if ($taxonomy == null) {
            $getCats = get_the_category($post_id);
        } else {
            $getCats = get_the_terms($post_id, $taxonomy);
        }

        if (is_array($getCats)) {
            if ($no_icon == null) {
                $html = '';
                foreach ($getCats as $key => $cat) {
                    $html .= ' <small><a href="' . get_category_link($cat->term_id) . '"><i class="fa fa-folder-open-o"></i> ' . " " . $cat->name . ' </a> </small>';
                }
                $html .= '</ul>';
            } else {
                $html = '<ul class="article-meta">';
                foreach ($getCats as $key => $cat) {
                    $html .= '<li>' . $cat->name . '</li>';
                }
                $html .= '</ul>';
            }
        } else {
            $html = "";
        }
        return balanceTags($html);
    }

    /**
     * Template for tags lists.
     *
     * @param $post_id
     */
    static function get_tags_lists($post_id)
    {
        $getTerms = wp_get_post_terms($post_id);
        $count = count($getTerms);
        $html = '';
        if ($count > 0) {
            $html .= '';
            foreach ($getTerms as $key => $value) {
                $html .= ' <a href="' . get_tag_link($value->term_id) . '">' . trim($value->name) . '</a>';
            }
            $html .= '</ul>';
        }
        return balanceTags($html);
    }

    /*
     * Return Social Icons From Theme options
     *
     */

    static function wpbucket_generate_social_html()
    {
        $html = '<div class="bottom-element-wrapper">
                        <ul class="social-links">';
        $social = wpbucket_return_theme_option('header_social_icons', 'icon');
        $social_url = wpbucket_return_theme_option('header_social_icons', 'text');
        foreach ($social as $index => $icon) {
            $html .= '<li>
                            <a href="' . esc_url($social_url[$index]) . '"><i class="' . esc_attr($icon) . '"></i></a>
	                  </li>';
        }
        $html .= '</ul></div>';

        return balanceTags($html);
    }

}
    