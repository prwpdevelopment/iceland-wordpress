<?php

/* ---------------------------------------------------------
 * Ations
 *
 * Class for registering filters
  ---------------------------------------------------------- */

class Wpbucket_Theme_Filters {

    static $hooks = array();

    /**
     * Initialize filters
     */
    static function init() {

        /**
         * Array of filter hooks
         * 
         * Usage:
         * 
         * 'filter_name' => array(
         *      'callback' => array( priority, accepted_args )
         * )
         * 
         * When 'callback' value is empty (non-array) or any of values ommited, 
         * default priority and accepted args will be used
         * 
         * e.g.
         * priority = 10
         * accepted_args = 1
         */
        static::$hooks = array(
            // WORDPRESS FILTERS
            'admin_body_class' => array(
                'Wpbucket_Theme_Filters::admin_body_class'
            ),
            'excerpt_length' => array(
                'Wpbucket_Theme_Filters::excerpt_length'
            ),
            'wp_page_menu_args' => array(
                'Wpbucket_Theme_Filters::page_menu_args'
            ),
            /* 'widget_tag_cloud_args' => array(
              'Wpbucket_Theme_Filters::wpbucket_tag_cloud_args'
              ), */
            'comment_form_defaults' => array(
                'Wpbucket_Theme_Filters::get_comment_form'
            ),
            'tiny_mce_before_init' => array(
                'Wpbucket_Theme_Filters::add_tinymce_tables'
            ),
            'override_load_textdomain' => array(
                'Wpbucket_Theme_Filters::override_load_textdomain' => array( 5, 3 )
            ),
            // THEME AND FRAMEWORK FILTERS
            'vcpt_register_custom_post_types' => array(
                'Wpbucket_Theme_Filters::register_custom_post_types'
            ),
            'wpbucket_blog_style' => array(
                'Wpbucket_Theme_Filters::search_page_blog_style'
            ),
            'wpbucket_main_menu_location' => array(
                'Wpbucket_Theme_Filters::main_menu_name'
            ),
            'masterslider_disable_auto_update' => array(
                'Wpbucket_Theme_Filters::disable_master_slider_update_notifications'
            ),
        );

        if ( shortcode_exists( 'ssba' ) ) {
            static::$hooks['ssba_html_output'] = array(
                'Wpbucket_Theme_Filters::remove_ssba_from_content' => array( 10, 2 )
            );
        }

        // register filters
        wpbucket_register_hooks( static::$hooks, 'filter' );
    }

    /**
     * Filter post types configuration where we register
     * post types that are needed in theme
     * 
     * @param array $post_types_config Array with post types
     * @return array Array with configuration
     */
    static function register_custom_post_types( $post_types_config ) {

        $post_types_config = array(
            'pi_catering' => array(
                'cpt' => '1',
                'taxonomy' => '1'
            ),
            'pi_gallery' => array(
                'cpt' => '1',
                'taxonomy' => '1'
            ),
            'pi_menus' => array(
                'cpt' => '1',
                'taxonomy' => '1'
            )
        );

        return $post_types_config;
    }

    /**
     * Title customization
     * 
     * @global int $page
     * @global int $paged
     * @global object $post
     * @param string $title
     * @param string $sep
     * @return string
     */
    static function wp_title( $title, $sep ) {
        if ( is_feed() ) {
            return $title;
        }

        global $page, $paged, $post;

        $title_name = get_bloginfo( 'name', 'display' );
        $site_description = get_bloginfo( 'description', 'display' );

        if ( $site_description && (is_home() || is_front_page()) ) {
            $title = "$title_name $sep $site_description";
        } elseif ( is_page() ) {
            $title = get_the_title( $post->ID );
            if ( ($paged >= 2 || $page >= 2) && !is_404() ) {
                $title .= " $sep " . sprintf( esc_html__( 'Page %s', 'wpbucket' ), max( $paged, $page ) );
            }
        } elseif ( ($paged >= 2 || $page >= 2) && !is_404() ) {
            $title = "$title_name $sep " . sprintf( esc_html__( 'Page %s', 'wpbucket' ), max( $paged, $page ) );
        } elseif ( is_author() ) {
            $author = get_queried_object();
            $title = $author->display_name;
        } elseif ( is_search() ) {
            $title = 'Search results for: ' . get_search_query() . '';
        }

        return $title;
    }

    /**
     * Overrides the load textdomain functionality when 'wpbucket' is the domain in use.  The purpose of
     * this is to allow theme translations to handle the framework's strings.  What this function does is
     * sets the 'wpbucket' domain's translations to the theme's.
     * 
     * @global type $l10n
     * @param boolean $override
     * @param type $domain
     * @param type $mofile
     * @return boolean
     */
    static function override_load_textdomain( $override, $domain, $mofile ) {

        if ( $domain == 'wpbucket' ) {
            global $l10n;

            $theme_text_domain = wpbucket_get_theme_textdomain();

            // If the theme's textdomain is loaded, use its translations instead.
            if ( $theme_text_domain && isset( $l10n[$theme_text_domain] ) )
                $l10n[$domain] = $l10n[$theme_text_domain];

            // Always override.  We only want the theme to handle translations.
            $override = true;
        }

        return $override;
    }

    /**
     * Enable Menu Name
     * 
     * @return Menu Name
     */
    static function main_menu_name() {

        return 'primary';
    }

    /**
     * Add class "wpbucket-portfolio-not-active" to create/edit page screen if 
     * Custom Post Types plugin isn't active
     * 
     * @global type $pagenow
     * @global type $typenow
     * @param string $classes Body classes to filter
     * @return string All body classes
     */
    static function admin_body_class( $classes ) {
        global $pagenow, $typenow;

        if ( !WPBUCKET_CPTS && is_admin() && ( $pagenow == 'post-new.php' || $pagenow == 'post.php' ) && $typenow == 'page' ) {
            $classes .= 'wpbucket-cpts-not-active';
        }

        return $classes;
    }

    /**
     * Sets the post excerpt length to 40 words.
     * 
     * To override this length in a child theme, remove the filter and add your own
     * function tied to the excerpt_length filter hook.
     * 
     * @param int $length
     * @return int
     */
    static function excerpt_length( $length ) {
        return 40;
    }

    /**
     * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
     * 
     * @param array $args
     * @return boolean
     */
    static function page_menu_args( $args ) {
        $args['show_home'] = true;
        return $args;
    }

    /**
     * Tags widget customizations
     * 
     * @param array $args
     * @return array
     */
    static function tag_cloud_args( $args ) {
        $args['smallest'] = 11;
        $args['largest'] = 11;
        $args['unit'] = "px";
        return $args;
    }

    /**
     * Comment Form styling
     * 
     * @param array $fields
     * @return array
     */
    static function get_comment_form( $fields ) {

        // get current commenter data
        $commenter = wp_get_current_commenter();

        // check if field is required
        $req = get_option( 'require_name_email' );
        $aria_req = ( $req ? " aria-required='true' required" : '' );

        // change fields style
        $fields['fields']['author'] = '<fieldset class="name-container"><label for="comment-name">' . esc_html__( 'Name:', 'wpbucket' ) . ($req ? ' <span class="text-color">*</span>' : '') . '</label>' .
                '<span class="comment-name-container comment-input-container"><input type="text" name="author" class="name" id="comment-name" value="' . esc_attr( $commenter['comment_author'] ) . '" size="22" tabindex="1"' . $aria_req . '/></span></fieldset>';

        $fields['fields']['email'] = '<fieldset class="email-container"><label for="comment-email">' . esc_html__( 'E-Mail:', 'wpbucket' ) . ($req ? ' <span class="text-color">*</span>' : '') . '</label>' .
                '<span class="comment-email-container comment-input-container"><input type="email" name="email" class="email" id="comment-email" value="' . esc_attr( $commenter['comment_author_email'] ) . '" size="22" tabindex="2" ' . $aria_req . '/></span></fieldset>';

        $fields['fields']['url'] = '';

        $fields['comment_field'] = '<fieldset class="message"><label for="comment-message">' . esc_html__( 'Message:', 'wpbucket' ) . ($req ? ' <span class="text-color">*</span>' : '') . '</label><span class="comment-message-container comment-input-container"><textarea name="comment" class="comment-text" id="comment-message" rows="8" tabindex="4" aria-required="true" required></textarea></span></fieldset>';

        $fields['comment_notes_before'] = '';
        $fields['comment_notes_after'] = '<p class="reguired-fields">' . esc_html__( 'Required fields are marked ', 'wpbucket' ) . '<span class="text-color">*</span></p>';
        $fields['cancel_reply_link'] = ' - ' . esc_html__( 'Cancel reply', 'wpbucket' );
        $fields['title_reply'] = esc_html__( 'Leave a comment', 'wpbucket' );
        $fields['id_submit'] = 'comment-reply';
        $fields['label_submit'] = esc_html__( 'Submit', 'wpbucket' );

        return $fields;
    }
    /**
     * Intercept Simple Share Buttons Adder output.
     * 
     * @param string $content
     * @param bool $using_shortcode
     * @return string
     */
    static function remove_ssba_from_content( $content, $using_shortcode ) {

        if ( !$using_shortcode && (is_page() || is_singular( 'pi_portfolio' )) ) {
            $content = "<section class='page-content'>"
                    . "<section class='container'>"
                    . "<div class='row'>"
                    . "<div class='col-md-12'>"
                    . $content
                    . "</div></seection></section>";
        }

        return $content;
    }

    /**
     * Edit Tinymce settings i.e. add custom classes for Tables in Editor
     * 
     * @param array $settings Tinymce settings
     * @return array
     */
    static function add_tinymce_tables( $settings ) {
        $new_styles = array(
            array(
                'title' => 'None',
                'value' => ''
            ),
            array(
                'title' => 'Events',
                'value' => 'events-table',
            )
        );
        $settings['table_class_list'] = json_encode( $new_styles );
        return $settings;
    }

    /**
     * Set blog style to "Large" for Search results page
     * 
     * @param string $style Blog style
     * @return string
     */
    static function search_page_blog_style( $style ) {

        // set Large blog style
        if ( is_search() ) {
            $style = 'blog-post-large';
        }

        return $style;
    }

    /**
     * Disabled Master Slider update notifications 
     * because user needs to have valid purchase code
     * 
     * @return string
     */
    static function disable_master_slider_update_notifications() {
        return true;
    }

}

Wpbucket_Theme_Filters::init();
