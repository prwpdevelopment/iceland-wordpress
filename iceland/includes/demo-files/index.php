<?php

// Exit if accessed directly
if (!defined('ABSPATH')) {
    die;
}

/**
 * Returns Redux options object
 *
 * @return string JSON object with Redux options
 */
function wpbucket_get_redux_demo_options()
{
    $redux = '{"last_tab":"","header_widget_areas":"3","logo":{"url":"http://thenordik.com/demo/themeforest/wordpress/iceland/wp-content/uploads/2016/11/logo.png","id":"316","height":"50","width":"180","thumbnail":"http://thenordik.com/demo/themeforest/wordpress/iceland/wp-content/uploads/2016/11/logo-150x50.png"},"top_drawer_on_off":"1","top_drawer_text":"Welcome to the <a href=\"#\">Iceland garden shop</a>. We offer landscaping for industrial clients.","custom_css":"","404_main_heading":"404","404_sub_heading":"Page Not Found","404_description":"The page you are looking for no longer exists. Perhaps you can return back to the sites homepage and see if you can find what you are looking for.","body_font2":{"font-family":"Droid Serif","font-options":"","google":"1","font-weight":"","font-style":"","subsets":"latin","text-align":"","font-size":"14px","line-height":"25px","color":"#787878"},"paragraphs":{"font-family":"Droid Serif","font-options":"","google":"1","font-weight":"","font-style":"","subsets":"latin","text-align":"","font-size":"","line-height":"25px","color":"#787878"},"heading_h1":{"font-family":"Raleway","font-options":"","google":"1","font-weight":"","font-style":"","subsets":"latin","text-align":"","font-size":"28px","line-height":"32px","color":"#212121"},"heading_h2":{"font-family":"Raleway","font-options":"","google":"1","font-weight":"","font-style":"","subsets":"latin","text-align":"","font-size":"24px","line-height":"28px","color":"#212121"},"heading_h3":{"font-family":"","font-options":"","google":"1","font-weight":"","font-style":"","subsets":"latin","text-align":"","font-size":"","line-height":"","color":"#212121"},"heading_h4":{"font-family":"Raleway","font-options":"","google":"1","font-weight":"","font-style":"","subsets":"latin","text-align":"","font-size":"18px","line-height":"22px","color":"#212121"},"heading_h5":{"font-family":"Raleway","font-options":"","google":"1","font-weight":"","font-style":"","subsets":"latin","text-align":"","font-size":"15px","line-height":"20px","color":"#212121"},"heading_h6":{"font-family":"Raleway","font-options":"","google":"1","font-weight":"","font-style":"","subsets":"latin","text-align":"","font-size":"13px","line-height":"18px","color":"#212121"},"footer_show":"1","footer_widget_areas":"3","copyright_text":"&copy; Iceland INC. All rights reserved 2016.","section-title-404":"","redux_options_object":"","redux_import_export":"","redux-backup":1}';

    return $redux;
}

/**
 * Returns array of predefined custom Newsletter forms
 *
 * @return string
 */
function wpbucket_get_newsletter_forms()
{
    $submit_url = esc_attr(home_url('/') . '?na=s');

    $forms = array(
        'form_1' => '<script type="text/javascript">
                    //<![CDATA[
                    if (typeof newsletter_check !== "function") {
                    window.newsletter_check = function (f) {
                        var re = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-]{1,})+\.)+([a-zA-Z0-9]{2,})+$/;
                        if (!re.test(f.elements["ne"].value)) {
                            alert("The email is not correct");
                            return false;
                        }
                        for (var i=1; i<20; i++) {
                        if (f.elements["np" + i] && f.elements["np" + i].required && f.elements["np" + i].value == "") {
                            alert("");
                            return false;
                        }
                        }
                        if (f.elements["ny"] && !f.elements["ny"].checked) {
                            alert("You must accept the privacy statement");
                            return false;
                        }
                        return true;
                    }
                    }
                    //]]>
                    </script>
                    
                     <div class="footer-newsletter clearfix">
                                                        <div class="input-group col-md-12">
                    <form method="post" action="' . $submit_url . '" onsubmit="return newsletter_check(this)">
                    
                    
                    <input class="form-control input-lg" type="email" name="ne" required>
                    
                            <input class="btn btn-primary btn-lg" type="submit" value="Go"/>
                    
                    </form>
                    </div>
                    </div>'
    );

    return $forms;
}

/**
 * Returns Widgets options object
 *
 * @return string JSON object with Widgets options
 */
function wpbucket_get_widget_demo_setup() {
    $widgets = '{"orphaned_widgets_1":{"search-2":{"title":""},"recent-posts-2":{"title":"","number":5},"recent-comments-2":{"title":"","number":5},"archives-2":{"title":"","count":0,"dropdown":0},"categories-2":{"title":"","count":0,"hierarchical":0,"dropdown":0},"meta-2":{"title":""}},"wpbucket-sidebar-5":{"tag_cloud-2":{"title":"","taxonomy":"post_tag"}},"blog-sidebar-id":{"search-3":{"title":"Search"},"recent-posts-3":{"title":"Popular Article","number":5,"show_date":true},"tag_cloud-3":{"title":"Tags","taxonomy":"post_tag"}},"wpbucket-sidebar-2":{"pi_tweet_scroll-2":{"title":"Latest Tweet\'s.","username":"themebasket","limit":10,"visible_tweets":3,"scroll_speed":600,"delay":3000,"time":"on","animation":"slide_up","url_new_window":"","logo":"","profile_image":"","caching":"0","consumer_key":"","consumer_secret":"","access_token":"","access_token_secret":""}},"wpbucket-sidebar-3":{"recent-posts-widget-with-thumbnails-2":{"title":"Recent Posts","default_url":"http:\/\/localhost\/iceland\/wp-content\/plugins\/recent-posts-widget-with-thumbnails\/default_thumb.gif","thumb_dimensions":"custom","category_ids":[0],"excerpt_length":55,"number_posts":2,"post_title_length":1000,"thumb_height":80,"thumb_width":80,"hide_current_post":false,"hide_title":false,"keep_aspect_ratio":false,"keep_sticky":false,"only_1st_img":false,"random_order":false,"show_author":false,"show_categories":false,"show_comments_number":false,"show_date":true,"show_excerpt":true,"ignore_excerpt":true,"show_thumb":true,"try_1st_img":false,"use_default":false,"open_new_window":true,"print_post_categories":false,"excerpt_more":" [\u2026]"}},"wpbucket-sidebar-4":{"text-3":{"title":"About Iceland.","text":"Island is an outstanding company who need gardening, landscaping design. When you need a gardener we are here for help with professional gardeners team. For more information and about prices, please visit our services page. Thanks for stop on Island garden company.","filter":true},"text-4":{"title":"","text":"[newsletter_form form=\"1\"]","filter":false}},"wpbucket-header-sidebar-2":{"wpbucket_widget_info-2":{"wpbcket_widget_icon":"flaticon-placeholder","wpbcket_widget_text":" 007 Edgar Buildings, George Street, CA 03, USA"}},"wpbucket-header-sidebar-3":{"wpbucket_widget_info-3":{"wpbcket_widget_icon":"flaticon-placeholder","wpbcket_widget_text":" 007 Edgar Buildings, George Street, CA 03, USA"}},"wpbucket-header-sidebar-4":{"wpbucket_widget_info-4":{"wpbcket_widget_icon":"flaticon-placeholder","wpbcket_widget_text":" 007 Edgar Buildings, George Street, CA 03, USA"}}}';

    return $widgets;
}
?>