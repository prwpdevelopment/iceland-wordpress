<?php

/**
 * Copyright (c) 06/11/2016.
 * Theme Name: iceland
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */
class Wpbucket_Widget_Info extends WP_Widget
{
    public function __construct()
    {
        // actual widget processes
        parent::__construct(
            'Wpbucket_Widget_Info', // Base ID
            __('Iceland Contact Info', 'wpbucket'), // Name
            array('description' => __('This widget is responsible to display contact information.', 'wpbucket'),) // Args
        );
    }

    public function widget($args, $instance)
    {
        // outputs the content of the widget

        echo '<div class="header-contact clearfix">
                            <p><i class="' . esc_attr($instance['wpbcket_widget_icon']) . ' alignleft"></i> ' . esc_html($instance['wpbcket_widget_text']) . '</p>
                        </div>';
    }

    public function form($instance)
    {
        // outputs the options form in the admin
        if (isset($instance['wpbcket_widget_icon'])) {
            $icon_default = $instance['wpbcket_widget_icon'];
        } else {
            $icon_default = 'flaticon-placeholder';
        }

        if (isset($instance['wpbcket_widget_text'])) {
            $text_default = $instance['wpbcket_widget_text'];
        } else {
            $text_default = ' 007 Edgar Buildings, George Street, CA 03, USA';
        }
        $icons = Wpbucket_Helpers::wpbucket_return_flaticon();

        $icon_radio = '';
        foreach ($icons as $icon) {
            if ($icon_default == $icon) {
                $icon_radio .= '<label class="label">
                <input type="radio" id="' . $this->get_field_id('wpbcket_widget_icon') . '" name="' . $this->get_field_name('wpbcket_widget_icon') . '" value="' . $icon . '" class="wpb_vc_select_flat_icon" checked="checked"/>
                <i class="' . $icon . '"></i>
                </label>';

            } else {
                $icon_radio .= '<label class="label">
                <input type="radio" id="' . $this->get_field_id('wpbcket_widget_icon') . '" name="' . $this->get_field_name('wpbcket_widget_icon') . '" value="' . $icon . '" class="wpb_vc_select_flat_icon"/>
                <i class="' . $icon . '"></i>
                </label>';
            }
        } ?>
        <p>
            <label><?php echo esc_html__('Choose Icon', 'wpbucket') ?></label>
            <br>
            <?php echo $icon_radio; ?>
            <br>
            <label><?php echo esc_html__('Text', 'wpbucket') ?></label>
            <textarea rows="10" cols="40" id="<?php echo $this->get_field_id('wpbcket_widget_text') ?>" name="<?php echo $this->get_field_name('wpbcket_widget_text') ?>"><?php echo esc_html( $text_default ); ?></textarea>
        </p>
        <?php
    }

    public function update($new_instance, $old_instance)
    {
        // processes widget options to be saved
        $instance = $old_instance;
        $instance['wpbcket_widget_icon'] = ( ! empty( $new_instance['wpbcket_widget_icon'] ) ) ? strip_tags( $new_instance['wpbcket_widget_icon'] ) : '';
        $instance['wpbcket_widget_text'] = ( ! empty( $new_instance['wpbcket_widget_text'] ) ) ? strip_tags( $new_instance['wpbcket_widget_text'] ) : '';
        return $instance;
    }
}