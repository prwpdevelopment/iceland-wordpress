<?php

/* ---------------------------------------------------------
 * Enqueue
 *
 * Class for including Javascript and CSS files
  ---------------------------------------------------------- */

class Wpbucket_Enqueue {

    public static $css;
    public static $js;
    public static $admin_css;

    /**
     * Configuration array for stylesheet that will be loaded
     */
    static function load_css() {

        // array with CSS file to load

        static::$css = array();

        // enqueue files
        Wpbucket_Enqueue::enqueue_css();
    }

    /**
     * Configuration array for Javascript files that will be loaded
     */
    static function load_js() {

        // hookname => array(filename, dependency_hook, load_in_footer)
        // url to file should be relative to the theme root directory
        // default: load_in_footer = TRUE
        static::$js = array(
            'wpbucket-bootstrap' => 'js/bootstrap.js',
            'wpbucket-custom' => 'js/custom.js',
        );

        // We add some JavaScript to pages with the comment form 
        // to support sites with threaded comments (when in use).         
        if ( is_singular() && get_option( 'thread_comments' ) ) {
            static::$js['comment-reply'] = '';
        }

        // enqueue files
        Wpbucket_Enqueue::enqueue_js();
    }

    /**
     * Enqueue Javascript and CSS file to admin
     */
    static function load_admin_css_js() {

        // array with admin css files
        static::$admin_css = array(
            'font-awesome' => WPBUCKET_TEMPLATEURL . '/includes/assets/font-awesome/css/font-awesome.min.css',
            'admin' => WPBUCKET_TEMPLATEURL . '/css/admin.css'
        );

        // enqueue files
        Wpbucket_Enqueue::enqueue_admin_css_js();
    }

    /**
     * Enqueue CSS files
     */
    static function enqueue_css() {

        // concate full url to file by add url prefix to css dir
        static::$css = array_map( 'wpbucket_enqueue_css_prefix', static::$css );

        // allow modifiying array of css files that will be loaded
        static::$css = apply_filters( 'wpbucket_css_files', static::$css );

        // loop through files and enqueue
        foreach ( static::$css as $key => $value ) {

            // if value is array it means dependency and $media might be set
            if ( is_array( $value ) ) {
                $file = isset( $value[0] ) ? $value[0] : '';
                $dependency = isset( $value[1] ) ? $value[1] : '';
                $media = isset( $value[2] ) ? $value[2] : 'all';

                wp_enqueue_style( $key, $file, $dependency, '', $media );
            } else {
                wp_enqueue_style( $key, $value, '', '' );
            }
        }
    }

    /**
     * Enqueue Javascript files
     */
    static function enqueue_js() {

        // concate full url to file by add url prefix to js dir
        static::$js = array_map( 'wpbucket_enqueue_js_prefix', static::$js );

        // allow modifiying array of javascript files that will be loaded
        static::$js = apply_filters( 'wpbucket_js_files', static::$js );

        // Enqueue Javascript
        wp_enqueue_script( 'jquery' );

        // loop through files and enqueue
        foreach ( static::$js as $key => $value ) {

            // if value is array it means dependency and $in_footer might be set
            if ( is_array( $value ) ) {
                $file = isset( $value[0] ) ? $value[0] : '';
                $dependency = isset( $value[1] ) ? $value[1] : '';
                $in_footer = isset( $value[2] ) ? $value[2] : true;

                wp_enqueue_script( $key, $file, $dependency, '', $in_footer );
            } else {
                wp_enqueue_script( $key, $value, '', '', true );
            }
        }
    }

    /**
     * Enqueue Javascript and CSS file to admin
     */
    static function enqueue_admin_css_js() {

        // allow modifiying array of css files that will be loaded
        static::$admin_css = apply_filters( 'wpbucket_admin_css_files', static::$admin_css );

        // loop through array of admin css files and load them
        foreach ( static::$admin_css as $key => $value ) {

            wp_enqueue_style( $key, $value );
        }
    }

    /**
     * Make certain options available on front-end
     */
    static function localize_script() {

        // pass settings to include.js file
      /*  $static_header = wpbucket_return_theme_option( 'static_header', '', '1' );
        $retina = wpbucket_return_theme_option( 'retina' );

        // output config object
        wp_localize_script( 'wpbucket_include', 'WpbucketConfig', array(
            'themeName' => sanitize_title( THEME_NAME ),
            'cmaActive' => CONTENT_MAKER ? '1' : '0',
            'staticHeader' => $static_header,
        ) );

        wp_localize_script( 'wpbucket_retina', 'WpbucketConfig', array(
            'retina' => $retina
        ) );*/
    }

}
