<?php

/*
 * ---------------------------------------------------------
 * Helpers
 *
 * Class for helper functions
 * ----------------------------------------------------------
 */

class Wpbucket_Helpers {
    /*
     * Return Image Sizes
     * parameter $name_thumbnail
     *
     */

    static function wpbucket_get_theme_related_image_sizes( $name_thubnail ) {
        global $vlc_is_retina;
        switch ( $name_thubnail ) {
            case 'blog_img' :
                $img_width = 360;
                $img_height = 234;
                break;
        }

        return array(
            'width' => $img_width,
            'height' => $img_height
        );
    }

    /*
     * Return Image Url
     * Parameter $post_id
     *
     */

    static function wpbucket_get_image_url( $post_id ) {
        $thumbnail = get_post_thumbnail_id( $post_id );
        $image_url = wp_get_attachment_url( $thumbnail, 'full' );

        return $image_url;
    }
    /*
       * Return Number of comments
       * Parameter $post_id
       *
       */
    static function wpbucket_get_comments_number($postid, $link=false){
        $num_comments = get_comments_number($postid); // get_comments_number returns only a numeric value

        if ( comments_open() ) {
            if ( $num_comments == 0 ) {
                $comments = __('No Comments','wpbucket');
            } elseif ( $num_comments > 1 ) {
                $comments = $num_comments . __(' Comments','wpbucket');
            } else {
                $comments = __('1 Comment','wpbucket');
            }
            if($link){
                $write_comments = $comments;
            }else{
                $write_comments = '<a href="' . get_comments_link() .'">'. $comments.'</a>';
            }

        } else {
            $write_comments =  __('Comments are off for this post.','wpbucket');
        }

        return $write_comments;
    }

    static function wpbucket_return_flaticon()
    {
        $flaticon = array(
            'flaticon-clock',
            'flaticon-contact-form',
            'flaticon-envelope',
            'flaticon-fence',
            'flaticon-forest',
            'flaticon-gardening',
            'flaticon-house',
            'flaticon-link',
            'flaticon-people',
            'flaticon-phone-call',
            'flaticon-placeholder',
            'flaticon-shop',
            'flaticon-shovel',
            'flaticon-shovel-1',
            'flaticon-sprout',
            'flaticon-swimming-pool',
            'flaticon-tree-leaf',
        );
        return $flaticon;
    }

}
