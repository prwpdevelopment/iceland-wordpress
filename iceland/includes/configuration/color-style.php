<?php

/*
 * ---------------------------------------------------------
 * Color style
 *
 * Class for custom color style rendering
 * ----------------------------------------------------------
 */
class Wpbucket_Color_Style {
	static function get_compiled_css($color_style) {
		$lighter_color = wpbucket_adjust_color_brightness ( $color_style, 4 );
		ob_start ();
		?>

        #header .navbar-default .navbar-nav > li > a:hover,
		#header .navbar-default .navbar-nav > .current-menu-item > a,
		#header .dropdown-menu > li.current-menu-item > a,
		#header .navbar-default .navbar-nav > li>a:hover, 
		#header .navbar-default .navbar-nav > li >a:focus, 
		#header .dropdown-menu > li > a:hover, 
		#header .dropdown-menu > li > a:focus, 
		#header .dropdown.dropdown-submenu > a:hover::after,
		#header .navbar-default .navbar-nav > .current-menu-item > a:before, #header .navbar-default .navbar-nav > li.current-menu-parent > a,#header .navbar-default .navbar-nav > li.current-menu-parent > a:before,
		.no-touch .dl-menuwrapper li a:hover,
		.dl-menuwrapper li.dl-back:after,
		.dl-menuwrapper li > a:not(:only-child):after,
		.dl-menuwrapper li.dl-back:after,
		strong a,
		.master-slider .pi-caption02,
		.page-content.dark .custom-heading .divider .hr-circle,
		.dark .custom-heading.style-1 h1 span,
		.dark .custom-heading.style-1 h2 span,
		.divider .divider-svg img,
		#footer-wrapper span,
		#footer-wrapper .social-links li a:hover i,
		#copyright-container .breadcrumb li a:hover,
		#copyright-container a,
		#copyright-container span,
		#fancy-testimonial-carousel .testimonial-author{
        color: <?php echo wp_kses( $color_style, array() ); ?>;
        }


        .search-submit,
        .wpcf7 .wpcf7-submit,
        .footer-social-icons li a:hover,
        .newsletter .newsletter-submit,
        .post-list .blog-post .post-date .month,
        .post-single .blog-post .post-date .month,
        .pagination li.active,
        .pagination li:hover,
        .scroll-up:hover,
        .pixely_widget_sharre .box .share:hover,
        .nivo-wrapper .nivo-directionNav a:hover,
        .blog-post.isotope-item .post-category a:hover,
        .comment-reply,
        .service-feature-box .service-media:hover a,
        .owl-carousel:hover .owl-controls .owl-nav .owl-prev:hover,
        .owl-carousel:hover .owl-controls .owl-nav .owl-next:hover,
        .owl-dots .owl-dot.active span,
        .owl-dots .owl-dot:hover span,
        .btn,
        .events-table .month,
        .accordion .title.active::after,
        .accordion .title:hover::after,
        .tracking .submit,
        .pi-latest-posts02 .post-date .month,
        .shipping-quote .submit,
        .custom-heading::after,
        .custom-heading02:after,
        .service-item-container figcaption h1:after,
        .service-item-container figcaption h2:after,
        .service-item-container figcaption h3:after,
        .service-item-container figcaption h4:after,
        .service-item-container figcaption h5:after,
        .gallery-item-container figcaption h1:after,
        .gallery-item-container figcaption h2:after,
        .gallery-item-container figcaption h3:after,
        .gallery-item-container figcaption h4:after,
        .gallery-item-container figcaption h5:after,
        .hover-details:hover span,
        .team-details:after,
        .hover-mask-container .hover-zoom:hover,
        .tabs li.active,
        .numbers-counter .counter-container::after,
        .master-slider .pi-button:hover,
        .master-slider .tooltip-wrap h6::after,
        .ms-skin-default .ms-tooltip-point .ms-point-center,
        .dropcap,
        .social-links.presentation li,
        #commentform #comment-reply,
        #reply-title:after{
        background-color: <?php echo wp_kses( $color_style, array() ); ?>;
        }


        /*  BORDERS
        ============================================================================= */
        .header-style01 .navbar-default .navbar-nav>li>a:hover,
        .header-style01 .nav > li.current-menu-item > a,
        .header-style01 .nav > li.current-menu-ancestor > a,
        .hover-mask-container .hover-details span,
        .hover-mask-container .hover-zoom,
        .master-slider .pi-button,
        .hr-simple.colored .hr-simple-circle,
        .dropcap.empty{
        border-color: <?php echo wp_kses( $color_style, array() ); ?>;
        }

        .dropdown .dropdown-menu .menu-item-has-children > a:hover:after,
        blockquote{
        border-left-color: <?php echo wp_kses( $color_style, array() ); ?>;
        }

        /*  LIGHTER COLOR
        ============================================================================= */
        /* Lighter base 01 */
        .search-submit:hover,
        .wpcf7 .wpcf7-submit:hover,
        .newsletter .newsletter-submit:hover,
        .comment-reply:hover,
        .service-feature-box .service-media:hover i,
        .btn:hover,
        .tracking .submit:hover,
        .shipping-quote .submit:hover,
        .social-links.presentation li:hover{
        background-color: <?php echo wp_kses( $lighter_color, array() ); ?>;
        }

        .icon-container .st1{
        fill: <?php echo wp_kses( $color_style, array() ); ?>;
        }

        <?php
		$css = ob_get_clean ();
		
		return $css;
	}
}
