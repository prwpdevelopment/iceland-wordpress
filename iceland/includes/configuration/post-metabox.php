<?php

/*
 * ---------------------------------------------------------
 * MetaBox
 *
 * Custom fields registration
 * ----------------------------------------------------------
 */

/*
 * --------------------------------------------------------------------
 * MetaBox for Custom Post Types
 * --------------------------------------------------------------------
 */

function wpbucket_register_meta_boxes( $meta_boxes ) {
    global $wpbucket_theme_config;

    // Get avaliable pages
    $all_avaliable_pages = get_pages( 'sort_column=post_parent,menu_order' );
    $pages [''] = 'Select a page';
    foreach ( $all_avaliable_pages as $page ) {
        $pages [$page->ID] = $page->post_title;
    }
    
    // get all registered sidebars in system
    $all_registered_sidebars = wpbucket_get_registered_sidebars();

    // Define a meta box for Pages
    $prefix = 'pg_';

    $meta_boxes [] = array(
        'title' => esc_html__( 'Page Options', 'wpbucket' ),
        'pages' => array(
            'page'
        ),
        // Works with Meta Box Tabs plugin
        'tabs' => array(
            'page_title' => esc_html__( 'Page title', 'wpbucket' ),
           // 'sidebar' => esc_html__( 'Sidebar', 'wpbucket' )
        ),
        'tab_wrapper' => true,
        'tab_style' => 'default',
        'fields' => array(            
            array(
                'name' => esc_html__( 'Hide page title', 'wpbucket' ),
                'id' => "{$prefix}hide_title",
                'desc' => esc_html__( 'Check this to hide page title.', 'wpbucket' ),
                'type' => 'checkbox',
                'std' => '0',
                'tab' => 'page_title'
            ),
            array(
                'name' => esc_html__( 'Nice title', 'wpbucket' ),
                'id' => "{$prefix}page_main_title",
                'desc' => esc_html__( 'Nice title will replace main page title. Leave empty if not needed.', 'wpbucket' ),
                'type' => 'text',
                'std' => '',
                'tab' => 'page_title'
            ),
            array(
                'name' => esc_html__( 'Nice Extra title', 'wpbucket' ),
                'id' => "{$prefix}page_sub_title",
                'desc' => esc_html__( 'Nice title will display after Nice Title. Leave empty if not needed.', 'wpbucket' ),
                'type' => 'text',
                'std' => '',
                'tab' => 'page_title'
            ),
            /*array(
                'name' => esc_html__( 'Background color', 'wpbucket' ),
                'desc' => esc_html__( 'Set title background color.', 'wpbucket' ),
                'id' => "{$prefix}title_color",
                'type' => 'color',
                'tab' => 'page_title'
            ),*/
            // Image Upload
            array(
                'name' => esc_html__( 'Background image', 'wpbucket' ),
                'desc' => esc_html__( 'Set background image..', 'wpbucket' ),
                'id' => "{$prefix}title_image",
                'type' => 'image_advanced',
                'max_file_uploads' => 1,
                'tab' => 'page_title'
            ),
            array(
                'name' => esc_html__( 'Parallax On/Off', 'wpbucket' ),
                'id' => "{$prefix}parallax_on_off",
                'desc' => esc_html__( 'Check this to on parallax.', 'wpbucket' ),
                'type' => 'checkbox',
                'std' => '0',
                'tab' => 'page_title'
            ),
            /*
             * Sidebar 
             */
           /* array(
                'name' => esc_html__( 'Sidebar placement', 'wpbucket' ),
                'desc' => esc_html__( 'Page can have left or right sidebar.', 'wpbucket' ),
                'id' => "{$prefix}sidebar",
                'type' => 'image_select',
                'std' => 'fullwidth',
                'options' => array(
                    'fullwidth' => WPBUCKET_TEMPLATEURL . '/core/assets/images/fullwidth.png',
                    'left' => WPBUCKET_TEMPLATEURL . '/core/assets/images/left.png',
                    'right' => WPBUCKET_TEMPLATEURL . '/core/assets/images/right.png'
                ),
                'tab' => 'sidebar',
            ),
            array(
                'name' => esc_html__( 'Sidebar generator', 'wpbucket' ),
                'desc' => esc_html__( 'Choose between new sidebar generation or selecting sidebar. We recommend Custom Sidebars plugin in case you want to generate new sidebar and use it for set of pages.', 'wpbucket' ),
                'id' => "{$prefix}sidebar_generator",
                'type' => 'select',
                'std' => 'new',
                'options' => array( 'new' => 'Generate new sidebar', 'existing' => 'Select existing sidebar' ),
                'tab' => 'sidebar',
                'visible' => array( 'pg_sidebar', "in", array( 'left', 'right' ) )
            ),
            array(
                'name' => esc_html__( 'Sidebar name', 'wpbucket' ),
                'desc' => esc_html__( 'Select sidebar you want to use on this page. If not set, Main Sidebar is used.', 'wpbucket' ),
                'id' => "{$prefix}sidebar_name",
                'type' => 'select',
                'std' => '',
                'options' => $all_registered_sidebars,
                'tab' => 'sidebar',
                'visible' => array( 'sidebar_generator', "=", 'existing' )
            ),*/
        )
    );

    return $meta_boxes;
}

add_filter( 'rwmb_meta_boxes', 'wpbucket_register_meta_boxes' );
?>
