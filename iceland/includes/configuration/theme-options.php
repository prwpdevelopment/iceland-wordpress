<?php

/*
 * ---------------------------------------------------------
 * Redux
 *
 * ReduxFramework Config File
 * ----------------------------------------------------------
 */
if ( !class_exists( 'Redux' ) ) {
    return;
}

// This is your option name where all the Redux data is stored.
$opt_name = "wpbucket_options";

$theme = wp_get_theme(); // For use with some settings. Not necessary.

$args = array(
    // TYPICAL -> Change these values as you need/desire
    'opt_name' => 'wpbucket_options',
    // This is where your data is stored in the database and also becomes your global variable name.
    'display_name' => $theme->get( 'Name' ),
    // Name that appears at the top of your panel
    'display_version' => $theme->get( 'Version' ),
    // Version that appears at the top of your panel
    'menu_type' => 'submenu',
    // Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
    'allow_sub_menu' => true,
    // Show the sections below the admin menu item or not
    'menu_title' => esc_html__( 'Theme Options', 'wpbucket' ),
    'page_title' => esc_html__( 'Theme Options', 'wpbucket' ),
    // You will need to generate a Google API key to use this feature.
    // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
    'google_api_key' => 'AIzaSyBsN1cG-NVXTUyefbmSlbv5NxMWyDzD8Nw',
    // Set it you want google fonts to update weekly. A google_api_key value is required.
    'google_update_weekly' => false,
    // Must be defined to add google fonts to the typography module
    'async_typography' => true,
    // Use a asynchronous font on the front end or font string
    // 'disable_google_fonts_link' => true, // Disable this in case you want to create your own google fonts loader
    'admin_bar' => true,
    // Show the panel pages on the admin bar
    'admin_bar_icon' => 'dashicons-portfolio',
    // Choose an icon for the admin bar menu
    'admin_bar_priority' => 50,
    // Choose an priority for the admin bar menu
    'global_variable' => '',
    // Set a different name for your global variable other than the opt_name
    'dev_mode' => false,
    // Show the time the page took to load, etc
    'update_notice' => true,
    // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
    'customizer' => true,
    // Enable basic customizer support
    // 'open_expanded' => true, // Allow you to start the panel in an expanded way initially.
    // 'disable_save_warn' => true, // Disable the save warning when a user changes a field
    // OPTIONAL -> Give you extra features
    'page_priority' => null,
    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
    'page_parent' => "wpbucket_options",
    // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
    'page_permissions' => 'manage_options',
    // Permissions needed to access the options panel.
    'menu_icon' => '',
    // Specify a custom URL to an icon
    'last_tab' => '',
    // Force your panel to always open to a specific tab (by id)
    'page_icon' => 'icon-themes',
    // Icon displayed in the admin panel next to your menu_title
    'page_slug' => '_vlc_options',
    // Page slug used to denote the panel
    'save_defaults' => true,
    // On load save the defaults to DB before user clicks save or not
    'default_show' => false,
    // If true, shows the default value next to each field that is not the default value.
    'default_mark' => '',
    // What to print by the field's title if the value shown is default. Suggested: *
    'show_import_export' => true,
    // Shows the Import/Export panel when not used as a field.
    // CAREFUL -> These options are for advanced use only
    'transient_time' => 60 * MINUTE_IN_SECONDS,
    'output' => true,
    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
    'output_tag' => true,
    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
    // 'footer_credit' => '', // Disable the footer credit of Redux. Please leave if you can help it.
    // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
    'database' => '',
    // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
    'system_info' => false
);

// SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
$args ['share_icons'] [] = array(
    'url' => 'https://github.com/pixel-industry/',
    'title' => 'Visit us on GitHub',
    'icon' => 'el-icon-github'
);
// 'img' => '', // You can use icon OR img. IMG needs to be a full URL.

$args ['share_icons'] [] = array(
    'url' => 'https://www.facebook.com/pixel.industry.themes',
    'title' => 'Like us on Facebook',
    'icon' => 'el-icon-facebook'
);
$args ['share_icons'] [] = array(
    'url' => 'https://twitter.com/pixel_industry',
    'title' => 'Follow us on Twitter',
    'icon' => 'el-icon-twitter'
);
$args ['share_icons'] [] = array(
    'url' => 'http://www.linkedin.com/company/redux-framework',
    'title' => 'Find us on LinkedIn',
    'icon' => 'el-icon-linkedin'
);
$args ['share_icons'] [] = array(
    'url' => 'http://dribbble.com/pixel_industry',
    'title' => 'Our Work on Dribbble',
    'icon' => 'el-icon-dribbble'
);
$args ['share_icons'] [] = array(
    'url' => 'https://www.behance.net/pixel-industry',
    'title' => 'Our Portfolio on Behance',
    'icon' => 'el-icon-behance'
);

Redux::setArgs( $opt_name, $args );

/*
 *
 * ---> START SECTIONS
 *
 */

// ACTUAL DECLARATION OF SECTIONS

Redux::setSection( $opt_name, array(
    'icon' => 'el-icon-tint',
    'title' => esc_html__( 'Appearance & Header', 'wpbucket' ),
    'fields' => array(
        array(
            'id' => 'header_widget_areas',
            'type' => 'slider',
            'required' => array(
                'footer_show',
                '=',
                '1'
            ),
            'title' => esc_html__( 'Widget areas', 'wpbucket' ),
            'subtitle' => esc_html__( 'Number of widget areas.', 'wpbucket' ),
            "default" => "3",
            "min" => "1",
            "step" => "1",
            "max" => "3"
        ),
        array(
            'id' => 'logo',
            'type' => 'media',
            'title' => esc_html__( 'Logo', 'wpbucket' ),
            'compiler' => 'true',
            'subtitle' => esc_html__( 'Upload logo for your website.', 'wpbucket' )
        ),
        array(
            'id' => 'top_drawer_on_off',
            'type' => 'switch',
            'title' => esc_html__( 'Show Drawer', 'wpbucket' ),
            'subtitle' => esc_html__( 'Check to show drawer on top of the page.', 'wpbucket' ),
            "default" => 1,
            'on' => 'Show',
            'off' => 'Hide'
        ),
        array(
            'id' => 'top_drawer_text',
            'type' => 'editor',
            'required' => array(
                'top_drawer_on_off',
                '=',
                '1'
            ),
            'title' => esc_html__( 'Drawer Text', 'wpbucket' ),
            'desc' => esc_html__( 'Text that will used in drawer section.', 'wpbucket' ),
            'default' => 'Welcome to the <a href="#">Iceland garden shop</a>. We offer landscaping for industrial clients.'
        ),
        array(
            'id' => 'custom_css',
            'type' => 'ace_editor',
            'title' => esc_html__( 'Custom CSS', 'wpbucket' ),
            'subtitle' => esc_html__( 'Quickly add some CSS to your theme by adding it to this block.', 'wpbucket' ),
            'mode' => 'css',
            'compiler' => true,
            'options' => array(
                'minLines' => 15
            )
        )
    )
) );


Redux::setSection( $opt_name, array(
    'icon' => 'el-icon-tint',
    'title' => esc_html__( '404', 'wpbucket' ),
    'fields' => array(
        // 404 PAGE
        array(
            'id' => 'section-title-404',
            'type' => 'section',
            'title' => esc_html__( '404 Page', 'wpbucket' ),
            'subtitle' => esc_html__( 'Page title on 404 page.', 'wpbucket' ),
            'indent' => false
        ), // Indent all options below until the next 'section' option is set.
        array(
            'id' => '404_main_heading',
            'type' => 'text',
            'title' => esc_html__( 'Main Heading', 'wpbucket' ),
            'desc' => esc_html__( 'Text that will used as main heading in page title.', 'wpbucket' ),
            'default' => '404'
        ),
        array(
            'id' => '404_sub_heading',
            'type' => 'text',
            'title' => esc_html__( 'Secondary Heading', 'wpbucket' ),
            'desc' => esc_html__( 'Text that will used after main heading in page title like secondary title.', 'wpbucket' ),
            'default' => 'Page Not Found'
        ),
        array(
            'id' => '404_description',
            'type' => 'textarea',
            'title' => esc_html__( 'Short Description', 'wpbucket' ),
            'desc' => esc_html__( 'Text that will used as short description of this page.', 'wpbucket' ),
            'default' => 'The page you are looking for no longer exists. Perhaps you can return back to the site\'s homepage and see if you can find what you are looking for.'
        )
    )
) );

Redux::setSection( $opt_name, array(
    'icon' => 'el-icon-font',
    'title' => esc_html__( 'Typography', 'wpbucket' ),
    'fields' => array(
        array(
            'id' => 'body_font2',
            'type' => 'typography',
            'title' => esc_html__( 'Body Font', 'wpbucket' ),
            'subtitle' => esc_html__( 'Specify the body font properties.', 'wpbucket' ),
            'google' => true,
            'default' => array(
                'color' => '#787878',
                'font-size' => '14px',
                'font-family' => 'Droid Serif',
                'font-weight' => 'Normal',
                'line-height' => '25px'
            ),
            'output' => array(
                'body'
            )
        ),
        array(
            'id' => 'paragraphs',
            'type' => 'typography',
            'title' => esc_html__( 'Paragraph Font', 'wpbucket' ),
            'subtitle' => esc_html__( 'Specify the paragraph font properties.', 'wpbucket' ),
            'google' => true,
            'default' => array(
                'color' => '#787878',
                'font-family' => 'Droid Serif',
                'font-weight' => 'Normal',
                'line-height' => '25px'
            ),
            'output' => array(
                'p'
            )
        ),
        array(
            'id' => 'heading_h1',
            'type' => 'typography',
            'title' => esc_html__( 'H1 Heading', 'wpbucket' ),
            'subtitle' => esc_html__( 'Specify the body font properties.', 'wpbucket' ),
            'google' => true,
            'output' => array(
                'h1'
            ),
            'default' => array(
                'color' => '#212121',
                'font-size' => '28px',
                'font-family' => 'Raleway',
                'line-height' => '32px'
            )
        ),
        array(
            'id' => 'heading_h2',
            'type' => 'typography',
            'title' => esc_html__( 'H2 Heading', 'wpbucket' ),
            'subtitle' => esc_html__( 'Specify the properties for H2.', 'wpbucket' ),
            'google' => true,
            'output' => array(
                'h2'
            ),
            'default' => array(
                'color' => '#212121',
                'font-size' => '24px',
                'font-family' => 'Raleway',
                'line-height' => '28px'
            )
        ),
        array(
            'id' => 'heading_h3',
            'type' => 'typography',
            'title' => esc_html__( 'H3 Heading', 'wpbucket' ),
            'subtitle' => esc_html__( 'Specify the properties for H3.', 'wpbucket' ),
            'google' => true,
            'output' => array(
                'h3'
            ),
            'default' => array(
                'color' => '#212121',
                'font-size' => '21px',
                'font-family' => 'Raleway',
                'line-height' => '25px'
            )
        ),
        array(
            'id' => 'heading_h4',
            'type' => 'typography',
            'title' => esc_html__( 'H4 Heading', 'wpbucket' ),
            'subtitle' => esc_html__( 'Specify the properties for H4.', 'wpbucket' ),
            'google' => true,
            'output' => array(
                'h4'
            ),
            'default' => array(
                'color' => '#212121',
                'font-size' => '18px',
                'font-family' => 'Raleway',
                'line-height' => '22px'
            )
        ),
        array(
            'id' => 'heading_h5',
            'type' => 'typography',
            'title' => esc_html__( 'H5 Heading', 'wpbucket' ),
            'subtitle' => esc_html__( 'Specify the properties for H5.', 'wpbucket' ),
            'google' => true,
            'output' => array(
                'h5'
            ),
            'default' => array(
                'color' => '#212121',
                'font-size' => '15px',
                'font-family' => 'Raleway',
                'line-height' => '20px',
                'text-transform' => 'uppercase'
            )
        ),
        array(
            'id' => 'heading_h6',
            'type' => 'typography',
            'title' => esc_html__( 'H6 Heading', 'wpbucket' ),
            'subtitle' => esc_html__( 'Specify the properties for H6.', 'wpbucket' ),
            'google' => true,
            'output' => array(
                'h6'
            ),
            'default' => array(
                'color' => '#212121',
                'font-size' => '13px',
                'font-family' => 'Raleway',
                'line-height' => '18px'
            )
        )
    )
) );

Redux::setSection( $opt_name, array(
    'icon' => 'el-icon-adjust-alt',
    'title' => esc_html__( 'Footer', 'wpbucket' ),
    'fields' => array(
        array(
            'id' => 'footer_show',
            'type' => 'switch',
            'title' => esc_html__( 'Show Footer', 'wpbucket' ),
            'subtitle' => esc_html__( 'Show Footer section.', 'wpbucket' ),
            "default" => 1,
            'on' => 'Show',
            'off' => 'Hide'
        ),
        array(
            'id' => 'footer_widget_areas',
            'type' => 'slider',
            'required' => array(
                'footer_show',
                '=',
                '1'
            ),
            'title' => esc_html__( 'Widget areas', 'wpbucket' ),
            'subtitle' => esc_html__( 'Number of widget areas.', 'wpbucket' ),
            "default" => "4",
            "min" => "1",
            "step" => "1",
            "max" => "4"
        ),
        array(
            'id' => 'copyright_text',
            'type' => 'textarea',
            'title' => esc_html__( 'Copyright Text', 'wpbucket' ),
            'subtitle' => esc_html__( 'Enter copyright text.', 'wpbucket' ),
            'validate' => 'html', // see http://codex.wordpress.org/Function_Reference/wp_kses_post
            'default' => '&copy; Iceland INC. All rights reserved 2016.'
        )
    )
) );
