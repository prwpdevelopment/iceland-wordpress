<?php
/**
 * Copyright (c) 02/11/2016.
 * Theme Name: iceland
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */


/**
 * Product Loop Items.
 *
 * @see woocommerce_template_loop_product_link_open()
 * @see woocommerce_template_loop_product_link_close()
 * @see woocommerce_template_loop_add_to_cart()
 * @see woocommerce_template_loop_product_thumbnail()
 * @see woocommerce_template_loop_product_title()
 * @see woocommerce_template_loop_category_link_open()
 * @see woocommerce_template_loop_category_title()
 * @see woocommerce_template_loop_category_link_close()
 * @see woocommerce_template_loop_price()
 * @see woocommerce_template_loop_rating()
 */
remove_action('woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open',10);
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close',10);
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
add_action('woocommerce_before_shop_loop_item', 'wpbucket_woocommerce_template_loop_product_link_open', 10);
add_action('woocommerce_after_shop_loop_item', 'wpbucket_woocommerce_template_loop_product_link_close', 5);
add_action( 'woocommerce_after_shop_loop_item_title', 'wpbucket_woocommerce_template_loop_rating', 15 );

remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail');
remove_action('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title');
add_action('woocommerce_before_shop_loop_item_title', 'wpbucket_woocommerce_template_loop_product_thumbnail', 10);
add_action('woocommerce_shop_loop_item_title', 'wpbucket_woocommerce_template_loop_product_title', 10);


remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper');
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end');
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb',20, 0);
add_action( 'woocommerce_before_main_content', 'wpbucket_woocommerce_output_content_wrapper', 10 );
add_action( 'woocommerce_after_main_content', 'wpbucket_woocommerce_output_content_wrapper_end', 10 );

/*
 * Apply
 * Filter To Image Size
 * */
add_filter( 'single_product_large_thumbnail_size', 'add_shop_single' );
function add_shop_single(){
    return 'full';
}
/**
 * Insert the opening anchor tag for products in the loop.
 */
if (!function_exists('wpbucket_woocommerce_template_loop_product_link_open')) {
    function wpbucket_woocommerce_template_loop_product_link_open()
    {

        echo '';

    }
}

/**
 * Insert the opening anchor tag for products in the loop.
 */
if (!function_exists('wpbucket_woocommerce_template_loop_product_link_close')) {
    function wpbucket_woocommerce_template_loop_product_link_close()
    {
        echo '';
    }
}

/*
 * RATTING
 * */
if (!function_exists('wpbucket_woocommerce_template_loop_rating')) {
    function wpbucket_woocommerce_template_loop_rating(){
        global $product;
        if ( $rating_html = $product->get_rating_html() ){
            return $rating_html;
        }
    }
}
if (!function_exists('wpbucket_woocommerce_template_loop_product_thumbnail')) {
    function wpbucket_woocommerce_template_loop_product_thumbnail()
    {

        global $product;
        //$image_size = apply_filters('single_product_archive_thumbnail_size', 'full');

        if (has_post_thumbnail()) {
           // $props = wc_get_product_attachment_props(get_post_thumbnail_id(), $post);
            $img_url = wp_get_attachment_url(get_post_thumbnail_id());
            echo '<div class="post-media entry">
                                <img src="'.esc_url($img_url).'" alt="" class="img-responsive img-rounded img-thumbnail">
                                <div class="magnifier">
                                    <div class="buttons">
                                        '. apply_filters( 'woocommerce_loop_add_to_cart_link',
                sprintf( '<a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s st"><span class="flaticon-shop"></span></a>',
                    esc_url( $product->add_to_cart_url() ),
                    esc_attr( isset( $quantity ) ? $quantity : 1 ),
                    esc_attr( $product->id ),
                    esc_attr( $product->get_sku() ),
                    esc_attr( isset( $class ) ? $class : 'button' ),
                    esc_html( $product->add_to_cart_text() )
                ),
                $product ).'
                                    </div>
                                </div>
                            </div>';
        } else {
            echo 'No Product Image selected';
        }
    }
}

if (!function_exists('wpbucket_woocommerce_template_loop_product_title')) {
    function wpbucket_woocommerce_template_loop_product_title(){
        global $product;
        echo '<div class="shop-meta"><small><a href="#">'.$product->get_price_html().'</a></small>'.wpbucket_woocommerce_template_loop_rating().'<h4><a href="'.get_the_permalink().'">' . get_the_title() . '</a></h4></div>';
    }
}

if (!function_exists('wpbucket_woocommerce_output_content_wrapper')) {
    function wpbucket_woocommerce_output_content_wrapper(){
        echo "<div class=\"col-md-9 col-sm-12 product_details\">";
    }
}
if (!function_exists('wpbucket_woocommerce_output_content_wrapper_end')) {
    function wpbucket_woocommerce_output_content_wrapper_end(){
        echo '</div>';
    }
}