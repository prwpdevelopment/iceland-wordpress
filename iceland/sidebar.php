<?php
/**
 * Copyright (c) 24/10/2016.
 * Theme Name: iceland
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */
$page_sidebar_id = 'blog-sidebar-id';
if (is_active_sidebar($page_sidebar_id)) :
    dynamic_sidebar($page_sidebar_id);
endif;