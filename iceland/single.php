<?php
/**
 * Copyright (c) 24/10/2016.
 * Theme Name: iceland
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */
defined('ABSPATH') or die("No script kiddies please!");

get_header();
get_template_part('menu-section');
get_template_part('page-title');
?>
<div class="section">
    <div class="container">
        <div class="row blog-wrapper">
            <div id="content" class="col-md-9">
                <?php if (have_posts()): the_post(); ?>
                    <div class="about-widget single-blog">
                        <div class="post-media entry">
                            <img src="<?php echo Wpbucket_Helpers::wpbucket_get_image_url(get_the_ID()) ?>" alt=""
                                 class="img-responsive img-rounded">
                        </div><!-- end media -->
                        <div class="small-title">
                            <small><i class="fa fa-clock-o"></i> <?php echo get_the_date(); ?></small>
                            <small><a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><i
                                        class="fa fa-user"></i> <?php echo get_the_author_meta('first_name'); ?></a>
                            </small>
                            <small><a href="#"><i
                                        class="fa fa-comments"></i> <?php echo Wpbucket_Helpers::wpbucket_get_comments_number(get_the_ID()) ?>
                                </a></small>
                            <?php echo Wpbucket_Partials::get_categories_lists(get_the_ID()) ?>
                            <h4 class="blog-post-title"><?php echo get_the_title(); ?></h4>
                        </div><!-- end small-title -->
                        <?php
                        the_content();
                        wp_link_pages(array(
                            'before' => '<div class="page-links"><span class="page-links-title">' . __('Pages:', 'wpbucket') . '</span>',
                            'after' => '</div>',
                            'link_before' => '<span>',
                            'link_after' => '</span>',
                            'pagelink' => '<span class="screen-reader-text">' . __('Page', 'wpbucket') . ' </span>%',
                            'separator' => '<span class="screen-reader-text">, </span>',
                        ));
                        ?>

                        <div class="tags clearfix">
                            <?php echo Wpbucket_Partials::get_tags_lists(get_the_ID()); ?>
                        </div><!-- end tags -->

                    </div>
                    <?php
                    edit_post_link(
                        sprintf(
                        /* translators: %s: Name of current post */
                            __('Edit<span class="screen-reader-text"> "%s"</span>', 'wpbucket'),
                            get_the_title()
                        ),
                        '<footer class="entry-footer"><span class="edit-link">',
                        '</span></footer><!-- .entry-footer -->'
                    );
                    ?>
                <?php endif; ?>
                <?php
                if (comments_open() || get_comments_number()) {
                    comments_template();
                }
                ?>
            </div>
            <div class="sidebar col-md-3">
                <?php get_sidebar();; ?>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();
?>
