<?php
/**
 * Copyright (c) 01/11/2016.
 * Theme Name: iceland
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */
?>
<div class="page-parallax parallax parallax-off" data-stellar-background-ratio="1"
     style="background-image:url('<?php echo WPBUCKET_TEMPLATEURL; ?>/uploads/page_parallax_02.jpg');">

</div>
<div class="page-title">
    <div class="container">
        <div class="section-title">
            <h3><?php the_archive_title();?></h3>
            <p class="lead"><?php the_archive_description();?></p>
        </div><!-- end title -->
    </div><!-- end container -->
</div>
