<?php
/**
 * Copyright (c) 24/10/2016.
 * Theme Name: iceland
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */

defined('ABSPATH') or die("No script kiddies please!");

$drawer_on_off = wpbucket_return_theme_option('top_drawer_on_off', '', '1');
$drawer_text = wpbucket_return_theme_option('top_drawer_text', '', '');

if ($drawer_on_off == 1) {
    ?>
    <div class="topbar-panel panel panel-primary hidden-xs">
        <span class="clickable"><i class="fa fa-angle-up"></i></span>
        <div class="panel-body">
            <div class="topbar">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <?php echo apply_filters('the_content',$drawer_text);?>
                        </div><!-- end left -->
                    </div><!-- end row -->
                </div><!-- enc ontainer -->
            </div><!-- end topbar -->
        </div>
    </div>
<?php } ?>
<header class="header">
    <!-- .container start -->
    <div class="container">
        <!-- .row start -->
        <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <?php echo Wpbucket_Partials::generate_logo_html(); ?>
            </div><!-- end col -->
            <?php
            $widget_areas = apply_filters('wpbucket_header_widget_areas', wpbucket_return_theme_option('header_widget_areas', '', '3'));

            for ($i = 1; $i <= $widget_areas; $i++) {
                $grid_size = round(8 / $widget_areas);
                ?>
                <div class="col-md-<?php echo esc_html($grid_size) ?> hidden-xs">
                    <?php
                    $sidebarid = 'wpbucket-header-sidebar-' . ($i + 1);
                    if (!dynamic_sidebar($sidebarid)) :
                        ?>
                        <div class="widget">
                            <div class="title">
                                <h3><?php esc_html_e('Widget', 'wpbucket') ?></h3>
                            </div>
                            <p><?php esc_html_e('This is widget area. Set widget here.', 'wpbucket') ?></p>
                        </div>
                        <?php
                    endif;
                    ?>
                </div>
            <?php } ?>
        </div>
        <!-- .navbar.pi-mega start -->
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="myNavbar">
                <?php
                echo Wpbucket_Partials::generate_menu_html();
                ?>

            </div>
        </nav>
    </div>
</header>