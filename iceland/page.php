<?php
/**
 * Copyright (c) 24/10/2016.
 * Theme Name: iceland
 * Author: wpbucket
 * Website: http://wordpressbucket.com/
 */
defined('ABSPATH') or die("No script kiddies please!");

get_header();
get_template_part('menu-section');
get_template_part('page-title');

?>

<?php if (wpbucket_has_shortcode('vc_row') != true && !is_page( 'cart' ) && !is_page( 'shop' ) && !is_page( 'checkout' ) && !is_page( 'my-account' )) { ?>
    <div class="section">
        <div class="container">
            <div class="row">
                <div id="content" class="col-md-9">
                    <?php if (have_posts()) : ?>

                        <?php
                        // Start the loop.
                        while (have_posts()) : the_post();

                            /*
                             * Include the Post-Format-specific template for the content.
                             * If you want to override this in a child theme, then include a file
                             * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                             */
                            get_template_part('template-parts/content', 'page');

                            // End the loop.
                        endwhile;

                    // If no content, include the "No posts found" template.
                    else :
                        get_template_part('template-parts/content', 'none');

                    endif;
                    ?>
                    <?php
                    if (comments_open() || get_comments_number()) {
                        comments_template();
                    }
                    ?>
                </div>
                <div class="sidebar col-md-3">
                    <?php get_sidebar(); ?>
                </div>
            </div>

        </div>
    </div>
<?php } elseif (WPBUCKET_WOOCOMMERCE && wpbucket_has_shortcode('vc_row') != true) { ?>
    <div class="section">
        <div class="container">
            <div class="row shoppanel">

                    <?php if (have_posts()) :

                        // Start the loop.
                        while (have_posts()) : the_post();

                            /*
                             * Include the Post-Format-specific template for the content.
                             * If you want to override this in a child theme, then include a file
                             * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                             */

                            the_content();

                            // End the loop.
                        endwhile;

                    // If no content, include the "No posts found" template.
                    else :
                        get_template_part('template-parts/content', 'none');

                    endif;

                    if (comments_open() || get_comments_number()) {
                        comments_template();
                    } ?>
        
            </div>
        </div>
    </div>
<?php } else {
    if (have_posts()) :

        // Start the loop.
        while (have_posts()) : the_post();

            /*
             * Include the Post-Format-specific template for the content.
             * If you want to override this in a child theme, then include a file
             * called content-___.php (where ___ is the Post Format name) and that will be used instead.
             */
            the_content();

            // End the loop.
        endwhile;

    // If no content, include the "No posts found" template.
    else :
        get_template_part('template-parts/content', 'none');

    endif;

    if (comments_open() || get_comments_number()) {
        comments_template();
    }
}

?>

<?php
get_footer();
?>